# Quadrant.Cpp

This tiny library introduces one simple class called `nstd::quadrant` and a few tuple-like utilities.
`nstd::quadrant` facilitates detailed (non-lexicographical) comparisons between sets of values of equal size
and allows to turn large chains of if/elses into switch statements. This can be illustrated as
```cpp
int x1 = 42;
double x2 = 3.14;

int y1 = 34;
double y2 = 34.0;

std::string z1 = "Hi!";
std::string z2 = "Lo.";

switch(nstd::quadrant::cmp(
        std::tuple{x1, y1, z1},
        std::tuple{x2, y2, z2}))
{
        case nstd::quadrant(-1, -1, -1): std::cout << "x1 <  x2, y1 <  y2, z1 <  z2\n"; break;
        case nstd::quadrant(-1, -1,  0): std::cout << "x1 <  x2, y1 <  y2, z1 == z2\n"; break;
        case nstd::quadrant(-1, -1,  1): std::cout << "x1 <  x2, y1 <  y2, z1 >  z2\n"; break;

        case nstd::quadrant(-1,  0, -1): std::cout << "x1 <  x2, y1 == y2, z1 <  z2\n"; break;
        case nstd::quadrant(-1,  0,  0): std::cout << "x1 <  x2, y1 == y2, z1 == z2\n"; break;
        case nstd::quadrant(-1,  0,  1): std::cout << "x1 <  x2, y1 == y2, z1 >  z2\n"; break;

        case nstd::quadrant(-1,  1, -1): std::cout << "x1 <  x2, y1 >  y2, z1 <  z2\n"; break;
        case nstd::quadrant(-1,  1,  0): std::cout << "x1 <  x2, y1 >  y2, z1 == z2\n"; break;
        case nstd::quadrant(-1,  1,  1): std::cout << "x1 <  x2, y1 >  y2, z1 >  z2\n"; break;

        case nstd::quadrant( 0, -1, -1): std::cout << "x1 == x2, y1 <  y2, z1 <  z2\n"; break;
        case nstd::quadrant( 0, -1,  0): std::cout << "x1 == x2, y1 <  y2, z1 == z2\n"; break;
        case nstd::quadrant( 0, -1,  1): std::cout << "x1 == x2, y1 <  y2, z1 >  z2\n"; break;

        case nstd::quadrant( 0,  0, -1): std::cout << "x1 == x2, y1 == y2, z1 <  z2\n"; break;
        case nstd::quadrant( 0,  0,  0): std::cout << "x1 == x2, y1 == y2, z1 == z2\n"; break;
        case nstd::quadrant( 0,  0,  1): std::cout << "x1 == x2, y1 == y2, z1 >  z2\n"; break;

        case nstd::quadrant( 0,  1, -1): std::cout << "x1 == x2, y1 >  y2, z1 <  z2\n"; break;
        case nstd::quadrant( 0,  1,  0): std::cout << "x1 == x2, y1 >  y2, z1 == z2\n"; break;
        case nstd::quadrant( 0,  1,  1): std::cout << "x1 == x2, y1 >  y2, z1 >  z2\n"; break;

        case nstd::quadrant( 1, -1, -1): std::cout << "x1 >  x2, y1 <  y2, z1 <  z2\n"; break;
        case nstd::quadrant( 1, -1,  0): std::cout << "x1 >  x2, y1 <  y2, z1 == z2\n"; break;
        case nstd::quadrant( 1, -1,  1): std::cout << "x1 >  x2, y1 <  y2, z1 >  z2\n"; break;

        case nstd::quadrant( 1,  0, -1): std::cout << "x1 >  x2, y1 == y2, z1 <  z2\n"; break;
        case nstd::quadrant( 1,  0,  0): std::cout << "x1 >  x2, y1 == y2, z1 == z2\n"; break;
        case nstd::quadrant( 1,  0,  1): std::cout << "x1 >  x2, y1 == y2, z1 >  z2\n"; break;

        case nstd::quadrant( 1,  1, -1): std::cout << "x1 >  x2, y1 >  y2, z1 <  z2\n"; break;
        case nstd::quadrant( 1,  1,  0): std::cout << "x1 >  x2, y1 >  y2, z1 == z2\n"; break;
        case nstd::quadrant( 1,  1,  1): std::cout << "x1 >  x2, y1 >  y2, z1 >  z2\n"; break;

	default: std::cout << "Impossible!\n";
}
```
this code prints `x1 >  x2, y1 == y2, z1 <  z2`. The `switch` statement in his example code can be regarded as largely equivalent to
```cpp
if(x1 < x2) {
        if(y1 < y2) {
                if(z1 < z2) {
                        std::cout << "x1 <  x2, y1 <  y2, z1 <  z2\n";
                }
                else if(z1 == z2) {
                        std::cout << "x1 <  x2, y1 <  y2, z1 == z2\n";
                }
                else {
                        std::cout << "x1 <  x2, y1 <  y2, z1 >  z2\n";
                }
        }
        else if(y1 == y2) {
                if(z1 < z2) {
                        std::cout << "x1 <  x2, y1 == y2, z1 <  z2\n";
                }
                else if(z1 == z2) {
                        std::cout << "x1 <  x2, y1 == y2, z1 == z2\n";
                }
                else {
                        std::cout << "x1 <  x2, y1 == y2, z1 >  z2\n";
                }
        }
        else { ... }
else if(x1 == x2) { ... }
else { ... }
```
except for each pair *var*1, *var*2 is compared exactly once, provided the three-way comparison is defined between their respective types,
and, worth mentioning, the switch-form could prove more compact or clean, or concise.

Currently all the definitions are contained inside the namespace `nstd`.

## Concepts

### `has_signum`
_Defined in the header_ **quadrant/quadrant.h**.
```cpp
template<class T> concept has_signum = /*unspecified*/;
```
The concept `nstd::has_signum` defines types of objects that can be passed to `nstd::quadrant`'s constructor. Currently this concept is limited to hold for arithmetic types and results of three-way comparison.

## Classes

### `quadrant`
_Defined in the header_ **quadrant/quadrant.h**.
```cpp
class quadrant;
```
This lightweight class is implicitly convertible to a scalar type `quadrant::value_type` that can itself be a `case` label inside a `switch` statement. The following members are defined.

#### Member types
```cpp
using value_type = /*unspecified*/;
```

An unsigned integral type capable to contain encoded results of at least a few three-way comparisons.

---------------

```cpp
using max_size = /*unspecied*/;
```
A value-containing type with `static constexpr` member `value` that specifies the maximum amount of three-way comparison results, or signs, `quadrant` can hold.

#### Member constants
```cpp
static constexpr auto max_size_v = max_size::value;
```

#### Constructor
```cpp
template<has_signum... Coords> requires(sizeof...(Coords) <= max_size_v)
constexpr quadrant(Coords... coords) noexcept(auto);
```
Every one of `Coords...` is either a number, or a three-way comparison result.

#### Assignment
```cpp
template<has_signum Coord> constexpr quadrant &operator=(Coord coord) noexcept(auto);
template<has_signum... Coords> constexpr quadrant &assign(Coords... coords) noexcept(auto);
```
All the previously held values (comparison results) are discarded, and new ones are stored inside this `quadrant`.

#### Implicit conversion
```cpp
constexpr operator value_type() const noexcept;
```
The value returned by this operator does not make any particular sense except for the following:

1. It encodes unambiguously a set of no more than `max_size_v` comparison results
2. As such it can be used as a `case` label in `switch`.

See the example above for how this allows to check for different (hyper)quadrants, and boundaries between those,
that set pairs can fall into when compared.

#### Static methods
```cpp
template<class Left, class Right>
static constexpr quadrant cmp(Left &&left, Right &&right) noexcept(auto);
```
`Left` and `Right` should be tuple-like types of equal `std::tuple_size` (that does not exceed `max_size`).

Return a `quadrant` that holds elementwise results of three-way comparison of `left` and `right` elements encoded. So, for instance, given
```cpp
int x1 = 0, x2 = 42;
std::string s1 = "Hi!", s2 = "Lo.";
```
(i.e. `x1` &lt; `x2` and `s1` &lt; `s2`), then the following expressins are `true`:
```
quadrant::cmp(std::pair{x1, s1}, std::pair{x2, s2}) == quadrant{-1, -1}
quadrant::cmp(std::pair{x1, s1}, std::pair{x2, s1}) == quadrant{-1,  0}
quadrant::cmp(std::pair{x1, s1}, std::pair{x1, s1}) == quadrant{ 0,  0}

quadrant::cmp(std::pair{x2, s2}, std::pair{x1, s1}) == quadrant{1, 1}
quadrant::cmp(std::pair{x2, s2}, std::pair{x1, s2}) == quadrant{1, 0}
quadrant::cmp(std::pair{x2, s2}, std::pair{x2, s2}) == quadrant{0, 0}
```

---------------

```cpp
template<class Left, class Right, class Comp> static constexpr quadrant cmp(Left &&left, Right &&right, Comp &&comp) noexcept(auto);
```
`Left` and `Right` should be tuple-like types of equal `std::tuple_size` (that does not exceed `max_size`).
A `quadrant` holding all the results of elementwise comparisons of `left`'s elements against those of `right` is returned.

`comp` is a comparison predicate. When invoked on tuples' elements,
it does not necessarily have to return a three-way comparison result/signed integer;
it can, for instance, return a boolean, and thus the `quadrant` returned would actually represent a vertex of a binary cube.

If
```cpp
int x1 = 0, x2 = 42;
std::string s1 = "Hi!", s2 = "Lo.";
```
(i.e. `x1` &lt; `x2` and `s1` &lt; `s2`) then
```
quadrant::cmp(std::pair{x1, s1}, std::pair{x2, s2}, std::less<void>{}) == quadrant{1, 1}
quadrant::cmp(std::pair{x1, s1}, std::pair{x2, s1}, std::less<void>{}) == quadrant{1, 0}
quadrant::cmp(std::pair{x1, s1}, std::pair{x1, s1}, std::less<void>{}) == quadrant{0, 0}

quadrant::cmp(std::pair{x2, s2}, std::pair{x1, s1}, std::less_equal<void>{}) == quadrant{0, 0}
quadrant::cmp(std::pair{x2, s2}, std::pair{x1, s2}, std::less_equal<void>{}) == quadrant{0, 1}
quadrant::cmp(std::pair{x2, s2}, std::pair{x2, s2}, std::less_equal<void>{}) == quadrant{1, 1}
```

### Tuple-related types
_Defined in the header_ **quadrant/tuples.h**.

#### `inf`
```cpp
class inf;
```
Instances of this literal type compare equal to themselves and positive floating point infinity,
and greater than any other C++ value.  They can also be converted to any numeric type `R`,
returning either `std::numeric_limits<R>::infinity()` or `std::numeric_limits<R>::max()`,
whichever is available.

#### `repeat`
```cpp
template<class T, auto count = inf{}>
class repeat;
```
This class represents a lazy tuple concept: partial specializations of `std::tuple_size`,
`std::tuple_element` and overloads of `std::get` are defined for it yet it takes as much memory
as to store a single object of type T, that serves as the tuple element at any valid index.

##### Constructor
```cpp
constexpr repeat(T &&) noexcept(auto);
```
```cpp
template<auto count, class T>
constexpr repeat<T, count> make_repeat(T &&) noexcept(auto);
```
Stores the provided object internally to be used as every tuple element.

Note that if `T` is an lvalue reference type, the object is stored by reference,
and can become dangling.

`noexcept` iff `T` is an lvalue reference, or a nothrow-moveable type.

##### Partial specializations of templates in namespace `std`
```
template<class T, auto count> class tuple_size<nstd::repeat<T, count>>;
```
When `count` is a value of an integral type, provides a static constexpr member `value` of type
`size_t` equal to `count`.

When `count` is `inf{}`, provides a static constexpr member `value` of type `size_t`
equal to `numeric_limits<size_t>::max()`, thus incorrectly limiting the actual tuple size.

```
template<size_t i, class T, auto count>
class tuple_element<nstd::repeat<T, count>>;
```
For values of `i` that are less than `count` (which in case of `count == inf{}` is
every possible value of `size_t`), provides a member typedef `type` equal to `T`.

Not defined for values of `i` that are greater or equal to `count`.

```
template<size_t i, class T, auto count>
constexpr tuple_element_t<i, nstd::repeat<T, count>> &get(nstd::repeat<T, count> &) noexcept;

template<size_t i, class T, auto count>
constexpr tuple_element_t<i, nstd::repeat<T, count>> const &get(nstd::repeat<T, count> const &) noexcept;

template<size_t i, class T, auto count>
constexpr tuple_element_t<i, nstd::repeat<T, count>> &&get(nstd::repeat<T, count> &&) noexcept;

template<size_t i, class T, auto count>
constexpr tuple_element_t<i, nstd::repeat<T, count>> const &&get(nstd::repeat<T, count> const &&) noexcept;
```
For values of `i` that are less than `count` (which in case of `count == inf{}` is
every possible value of `size_t`), returns the respective reference to the stored object.

Not defined for values of `i` that are greater or equal to `count`.

#### `tuple_cat`
```cpp
template<class... Tuples>
class tuple_cat;
```
This class provides a (possibly) lightweight alternative to `std::tuple_cat`. Unlike the result of the latter, it is not
defined to be an `std::tuple`; on the contrary, it stores its construction arguments as passed (either by reference,
for parameters of lvalue reference types, or by value for any others), not destructuring them to elements. As a result,
it can store efficiently subtuples of very large sizes, or even infinite ones, like `nstd::repeat<T, inf{}>`.

##### Constructor
```cpp
constexpr tuple_cat(Tuples &&...) noexcept(auto);
```
```cpp
template<class... Tuples> tuple_cat(Tuples &&...) -> tuple_cat<Tuples...>;
```

##### Partial specializations of templates in namespace `std`
```
template<class... Tuples> class tuple_size<nstd::tuple_cat<Tuples...>>;
```
Provides a static constexpr member `value` of type `size_t` equal to the sum of `tuple_size_v` of all `Tuples`.
If any of the tuples is infinite (that is, its `ntuple_size` is `inf{}`), `value` is `numeric_limits<size_t>::max()`.

```
template<size_t i, class... Tuples>
class tuple_element<nstd::tuple_cat<Tuples...>>;
```
For template parameters Tuple<sub>1</sub>, Tuple<sub>2</sub>, ..., with elements T<sub>i,j</sub>,
where tuple elements are defined as:
* for a tuple-like type `Tuple_i` (the one for which `std::tuple_size_v` is defined),
  T<sub>i,j</sub> is exactly `std::tuple_element_t<j, Tuple_i>`
* otherwise, for a non-tuple-like parameter `NonTuple` j ∈ {1}, and T<sub>i,1</sub> is the `NonTuple` itself,

`tuple_element<nstd::tuple_cat<Tuple_1, ..., NonTuple_i, ..., Tuple_j, ...>>` is equivalent to
`tuple_element<tuple<T_1_1, T_1_2, ..., T_2_1, ...>`, as if all the elements of tuples and non-tuples
were spliced into a single tuple, left to right, in order of template parameters and their elements.

Not defined for values of `i` that are greater or equal to `tuple_size_v`.

```
template<size_t i, class... Tuples>
constexpr tuple_element_t<i, nstd::tuple_cat<Tupes...>> &get(nstd::tuple_cat<Tupes...> &) noexcept;

template<size_t i, class... Tuples>
constexpr tuple_element_t<i, nstd::tuple_cat<Tupes...>> const &get(nstd::tuple_cat<Tupes...> const &) noexcept;

template<size_t i, class... Tuples>
constexpr tuple_element_t<i, nstd::tuple_cat<Tupes...>> &&get(nstd::tuple_cat<Tupes...> &&) noexcept;

template<size_t i, class... Tuples>
constexpr tuple_element_t<i, nstd::tuple_cat<Tupes...>> const &&get(nstd::tuple_cat<Tupes...> const &&) noexcept;
```
For values of `i` that are less than `count`, returns the respective reference to the respective element,
as defined in the description of the specialization of `tuple_element`.

Not defined for values of `i` that are greater or equal to `tuple_size_v`.

## INSTALL

To build tests, invoke
```sh
make tests
```
or simply
```sh
make
```
An executable `test` should appear in the `build` directory.

As this library is include-only, it does not require any particular build stage. To install the library in your **/usr/local**, invoke
```sh
sudo make install
```
