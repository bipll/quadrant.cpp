#include <cassert>
#include <functional>
#include <iostream>
#include <string>
#include <tuple>

#include <quadrant/quadrant.h>

#include "test.h"

TEST(test_quadrant) {
	std::size_t rv{};
	switch(nstd::quadrant(-2, 0, 3)) {
		case nstd::quadrant(-1, -1, -1): rv = 1; break;
		case nstd::quadrant(-1, -1, 0): rv = 2; break;
		case nstd::quadrant(-1, -1, 1): rv = 3; break;
		case nstd::quadrant(-1, 0, -1): rv = 4; break;
		case nstd::quadrant(-1, 0, 0): rv = 5; break;
					       // signs of (-2, 0, 3): (-1, 0, 1)
		case nstd::quadrant(-1, 0, 1): rv = 6; break;
		case nstd::quadrant(-1, 1, -1): rv = 7; break;
		case nstd::quadrant(-1, 1, 0): rv = 8; break;
		case nstd::quadrant(-1, 1, 1): rv = 9; break;
		case nstd::quadrant(0, -1, -1): rv = 10; break;
		case nstd::quadrant(0, -1, 0): rv = 11; break;
		case nstd::quadrant(0, -1, 1): rv = 12; break;
		case nstd::quadrant(0, 0, -1): rv = 13; break;
		case nstd::quadrant(0, 0, 0): rv = 14; break;
		case nstd::quadrant(0, 0, 1): rv = 15; break;
		case nstd::quadrant(0, 1, -1): rv = 16; break;
		case nstd::quadrant(0, 1, 0): rv = 17; break;
		case nstd::quadrant(0, 1, 1): rv = 18; break;
		case nstd::quadrant(1, -1, -1): rv = 19; break;
		case nstd::quadrant(1, -1, 0): rv = 20; break;
		case nstd::quadrant(1, -1, 1): rv = 21; break;
		case nstd::quadrant(1, 0, -1): rv = 22; break;
		case nstd::quadrant(1, 0, 0): rv = 23; break;
		case nstd::quadrant(1, 0, 1): rv = 24; break;
		case nstd::quadrant(1, 1, -1): rv = 25; break;
		case nstd::quadrant(1, 1, 0): rv = 26; break;
		case nstd::quadrant(1, 1, 1): rv = 27; break;
		default: rv = 28;
	}
	ASSERT_EQ(6, rv);
};

TEST(test_cmp) {
	std::size_t rv{};
	switch(nstd::quadrant::cmp(std::tuple{42, std::string("Hi!")}, std::tuple{3.14, std::string("Lo.")})) {
		case nstd::quadrant(-1, -1): rv = 1; break;
		case nstd::quadrant(-1, 0): rv = 2; break;
		case nstd::quadrant(-1, 1): rv = 3; break;
		case nstd::quadrant(0, -1): rv = 4; break;
		case nstd::quadrant(0, 0): rv = 5; break;
		case nstd::quadrant(0, 1): rv = 6; break;
					   // 42 > 3.14, "Hi!" < "Lo."
		case nstd::quadrant(1, -1): rv = 7; break;
		case nstd::quadrant(1, 0): rv = 8; break;
		case nstd::quadrant(1, 1): rv = 9; break;
		default: rv = 10;
	}
	ASSERT_EQ(7, rv);

	switch(nstd::quadrant::cmp({42, 314}, {42, 271})) {
		case nstd::quadrant(-1, -1): rv = 1; break;
		case nstd::quadrant(-1, 0): rv = 2; break;
		case nstd::quadrant(-1, 1): rv = 3; break;
		case nstd::quadrant(0, -1): rv = 4; break;
		case nstd::quadrant(0, 0): rv = 5; break;
					   // 42 == 3*14, 314 > 271
		case nstd::quadrant(0, 1): rv = 6; break;
		case nstd::quadrant(1, -1): rv = 7; break;
		case nstd::quadrant(1, 0): rv = 8; break;
		case nstd::quadrant(1, 1): rv = 9; break;
		default: rv = 10;
	}
	ASSERT_EQ(6, rv);

	switch(nstd::quadrant::cmp({42, 314, 13}, {42, 271, 37})) {
		case nstd::quadrant(-1, -1, -1): rv = 1; break;
		case nstd::quadrant(-1, -1, 0): rv = 2; break;
		case nstd::quadrant(-1, -1, 1): rv = 3; break;
		case nstd::quadrant(-1, 0, -1): rv = 4; break;
		case nstd::quadrant(-1, 0, 0): rv = 5; break;
		case nstd::quadrant(-1, 0, 1): rv = 6; break;
		case nstd::quadrant(-1, 1, -1): rv = 7; break;
		case nstd::quadrant(-1, 1, 0): rv = 8; break;
		case nstd::quadrant(-1, 1, 1): rv = 9; break;
		case nstd::quadrant(0, -1, -1): rv = 10; break;
		case nstd::quadrant(0, -1, 0): rv = 11; break;
		case nstd::quadrant(0, -1, 1): rv = 12; break;
		case nstd::quadrant(0, 0, -1): rv = 13; break;
		case nstd::quadrant(0, 0, 0): rv = 14; break;
		case nstd::quadrant(0, 0, 1): rv = 15; break;
					      // 42 == 3*14, 314 > 271, 13 < 37
		case nstd::quadrant(0, 1, -1): rv = 16; break;
		case nstd::quadrant(0, 1, 0): rv = 17; break;
		case nstd::quadrant(0, 1, 1): rv = 18; break;
		case nstd::quadrant(1, -1, -1): rv = 19; break;
		case nstd::quadrant(1, -1, 0): rv = 20; break;
		case nstd::quadrant(1, -1, 1): rv = 21; break;
		case nstd::quadrant(1, 0, -1): rv = 22; break;
		case nstd::quadrant(1, 0, 0): rv = 23; break;
		case nstd::quadrant(1, 0, 1): rv = 24; break;
		case nstd::quadrant(1, 1, -1): rv = 25; break;
		case nstd::quadrant(1, 1, 0): rv = 26; break;
		case nstd::quadrant(1, 1, 1): rv = 27; break;
		default: rv = 28;
	}
	ASSERT_EQ(16, rv);

	switch(nstd::quadrant::cmp({1, 2, 3, 4, 5, 6}, {1, 2, 3, 40, 5, 6})) {
		case nstd::quadrant(0, 0, 0, -1, 0, 0): rv = 42; break;
		default: rv = 0;
	}
	ASSERT_EQ(42, rv);
};

TEST(test_less) {
	std::size_t rv{};
	switch(nstd::quadrant::cmp(std::tuple{42, std::string("Hi!")}, std::tuple{3.14, std::string("Lo.")}, std::less<void>{})) {
		case nstd::quadrant(0, 0): rv = 1; break;
		case nstd::quadrant(0, 1): rv = 2; break;
		case nstd::quadrant(1, 0): rv = 3; break;
		case nstd::quadrant(1, 1): rv = 4; break;
		default: rv = 5;
	}
	ASSERT_EQ(2, rv);
	switch(nstd::quadrant::cmp(std::tuple{42, std::string("Hi!")}, std::tuple{42, std::string("Lo.")}, std::less<void>{})) {
		case nstd::quadrant(0, 0): rv = 1; break;
		case nstd::quadrant(0, 1): rv = 2; break;
		case nstd::quadrant(1, 0): rv = 3; break;
		case nstd::quadrant(1, 1): rv = 4; break;
		default: rv = 5;
	}
	ASSERT_EQ(2, rv);
	switch(nstd::quadrant::cmp(std::tuple{3.14, std::string("Hi!")}, std::tuple{42, std::string("Lo.")}, std::less<void>{})) {
		case nstd::quadrant(0, 0): rv = 1; break;
		case nstd::quadrant(0, 1): rv = 2; break;
		case nstd::quadrant(1, 0): rv = 3; break;
		case nstd::quadrant(1, 1): rv = 4; break;
		default: rv = 5;
	}
	ASSERT_EQ(4, rv);
};

TEST(test_mask) {
	std::size_t rv{};
	switch(auto q = nstd::quadrant::cmp({42, 314, 1337}, {42, 314, 271})) {
		case nstd::quadrant(0, 0, 0): rv = 1; break;
		case nstd::quadrant(0, 1, -1): rv = 2; break;
		case nstd::quadrant(1, 0, -1): rv = 4; break;
		default: if(q == nstd::quadrant(0, 0)) rv = 3; else rv = 5;
	}
	ASSERT_EQ(3, rv);
};
