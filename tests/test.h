#ifndef TESTS__TEST_H_c643909734d8d29404214642a42eb762708cdb2b
#define TESTS__TEST_H_c643909734d8d29404214642a42eb762708cdb2b

#ifdef NDEBUG
#undef NDEBUG
#endif

#include <cassert>
#include <functional>
#include <iostream>

#if __cpp_lib_ranges_to_container < 202202L
#include <numeric>
#endif

#include <ranges>
#include <stack>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

namespace nstd::test {

template<class T>
requires(requires(std::ostream s, T t) { { s << t } -> std::same_as<std::ostream &>; })
inline std::ostream &output(std::ostream &s, T const &t) {
	return s << t;
}

template<class A, class B>
inline std::ostream &output(std::ostream &s, std::pair<A, B> const &p) {
	return s << "std::pair{" << p.first << ", " << p.second << '}';
}

namespace impl_ {

template<class T, std::size_t... is>
inline std::ostream &outputTuple(std::ostream &s, T const &t, std::index_sequence<is...>) {
	s << "std::tuple{";
	((s << (is? ", " : "") << std::get<is>(t)), ...);
	return s << '}';
}

}

template<class... Types>
inline std::ostream &output(std::ostream &s, std::tuple<Types...> const &t) {
	return impl_::outputTuple(s, t, std::index_sequence_for<Types...>{});
}


template<class... Ts>
inline std::ostream &output(std::ostream &s, Ts const &...ts) {
	return (output(s, ts), ...);
}


struct AssertionContext {
	char const *file;
	int line;
	std::ptrdiff_t id{-1};
};

inline std::ostream &operator<<(std::ostream &s, AssertionContext const &ctx) {
	s << ctx.file << ':' << ctx.line << ": assertion failed";
	if(ctx.id >= 0) s << " at test case " << ctx.id;
	return s;
}


struct Test {
	static auto &addTest(std::string name) {
		index.emplace(name, tests.size());
		auto &rv = tests.emplace_back(std::move(name), nullptr);
		return rv.second;
	}

	static void run(auto &&names) {
		std::size_t cnt{};
		for(auto const &n: names) {
			if(auto itr = index.find(n); itr != index.end()) {
				std::cout << n << "()\n";
				tests[itr->second].second();
				++cnt;
			}
		}
		std::cout << cnt << " tests passed.\n";
	}

	static void runAll() {
		for(auto const &t: tests) {
			std::cout << t.first << "()\n";
			t.second();
		}
		std::cout << "All tests passed.\n";
	}

	static void assertEq(AssertionContext ctx, char const *argString, auto &&expected, auto &&...actual) {
		if(sizeof...(actual) == 0) return;
		auto equality = mergeArgs(argString, " == ");
		(compare(ctx, std::equal_to{}, equality, expected, actual), ...);
	}

private:
	static std::string mergeArgs(char const *argString, std::string_view sign) {
		static const std::unordered_map<char, char> pairs{
			{'(', ')'},
			{'[', ']'},
			{'{', '}'},
			{'<', '>'}
		};
		std::stack<char> closures, greatlessClosures;
		std::vector<std::string_view> args;
		std::size_t sz{};
		char const *init = argString;
		char const *edge = argString;
		for(; *argString; ++argString) {
			char c = *argString;
			if(auto itr = pairs.find(c); itr != pairs.end()) {
				closures.push(itr->second);
				if(c != '<') greatlessClosures.push(itr->second);
			}
			else if(!closures.empty() && c == closures.top()) {
				closures.pop();
				if(c != '>') greatlessClosures.pop();
			}
			else if(!greatlessClosures.empty() && closures.top() == '>' && c == greatlessClosures.top()) {
				while(closures.top() == '>') closures.pop();
				closures.pop();
				greatlessClosures.pop();
			}
			else if(greatlessClosures.empty() && c == ',') {
				args.emplace_back(init, edge + 1);
				if(!*++argString) break;
				closures = {};
				init = argString;
				while(*init && *init == ' ') ++init;
				edge = init;
				if(!*init) break;
				continue;
			}
			if(c == ' ') {
				if(init == argString) ++init;
			}
			else edge = argString;
		}
		if(init != edge) args.emplace_back(init, edge + 1);
#if __cpp_lib_ranges_to_container >= 202202L
		return std::ranges::to<std::string>(args | std::views::join_with(sign));
#else
		return std::accumulate(args.cbegin(), args.cend(), std::string{},
				[sign](std::string rv, std::string_view arg) {
					if(!rv.empty()) [[likely]] rv += sign;
					return std::move(rv += arg);
				});
#endif
	}

	static void compare(AssertionContext const &ctx, auto predicate, std::string const &assertion, auto &&expected, auto &&actual) {
		if(!predicate(expected, actual)) {
			output(std::cerr, ctx, ": ", assertion, "\n");
			output(std::cerr, "\tExpected: ", expected, '\n');
			output(std::cerr, "\tActual: ", actual, "\n\n");
		}
		assert(predicate(expected, actual));
	}

	inline static std::vector<std::pair<std::string, std::function<void()>>> tests;
	inline static std::unordered_map<std::string, std::size_t> index;
};

#define ASSERT_EQ(...) ::nstd::test::Test::assertEq({__FILE__, __LINE__}, #__VA_ARGS__, __VA_ARGS__)
#define ASSERT_EQ_AT(id, ...) ::nstd::test::Test::assertEq({__FILE__, __LINE__, id}, #__VA_ARGS__, __VA_ARGS__)

#define NSTD__CONCAT(A, B) NSTD__CONCAT_(A, B)
#define NSTD__CONCAT_(A, B) A ## B

#define TEST(name) inline auto const &NSTD__CONCAT(NSTD__TEST__OTHERWISE_IGNORED, name) = ::nstd::test::Test::addTest(#name) = []


template<auto const &v> struct ConstRef {
	static constexpr auto const &value = v;
};


template<auto v> using Constant = std::integral_constant<decltype(v), v>;


template<class... Vs> struct Values {
	using type = std::tuple<Vs...>;
};
template<class... Vs> using ValuesT = typename Values<Vs...>::type;

template<class V> struct Values<V> {
	using type = V;
};


template<auto k, class... Values> struct Mapped;

template<class T> struct Key;
template<class T> static constexpr auto KeyV = Key<T>::value;

template<auto k, class... Values> struct Key<Mapped<k, Values...>> {
	static constexpr auto value = k;
};

template<class... Entries> struct Map {};

template<auto, class> struct Find;
template<auto k, class Map> using FindT = typename Find<k, Map>::type;

template<auto k, class Entry, class... OtherEntries> struct Find<k, Map<Entry, OtherEntries...>>
	: Find<k, Map<OtherEntries...>> {};

template<auto k, auto k0, class... Vs, class... OtherEntries>
requires(k == k0)
struct Find<k, Map<Mapped<k0, Vs...>, OtherEntries...>>
	: Values<Vs...> {};


template<auto... values>
inline constexpr void static_for(auto &&f)
noexcept((noexcept(f.template operator()<values>()) && ...))
{
	(f.template operator()<values>(), ...);
}

template<class V, V... values>
inline constexpr void static_for(auto &&f, std::integer_sequence<V, values...>)
{
	static_for<values...>(f);
}

template<class... Entries>
inline constexpr void static_for(auto &&f, Map<Entries...>) {
	static_for<KeyV<Entries>...>(f);
}

namespace impl_::elementwise_same {

template<class Left, class Right, std::size_t... is>
inline constexpr bool same_at(std::index_sequence<is...>) noexcept
{
	return ((std::tuple_size_v<Left> == std::tuple_size_v<Right>) && ... &&
			(std::same_as<std::tuple_element_t<is, Left>, std::tuple_element_t<is, Right>>));
}

}

template<class Left, class Right>
concept elementwise_same = impl_::elementwise_same::same_at<Left, Right>(std::make_index_sequence<std::tuple_size_v<Left>>{});

}

#ifdef QUADRANT_CPP__TEST__MAIN
int main(int argc, char **argv) {
	if(argc > 1) {
		std::vector<std::string> tests(argv + 1, argv + argc);
		nstd::test::Test::run(tests);
	}
	else nstd::test::Test::runAll();
}
#endif

#endif
