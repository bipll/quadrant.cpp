#include <cassert>
#include <concepts>
#include <functional>
#include <iostream>
#include <string>
#include <tuple>

#include <quadrant/tuple.h>

#include "test.h"

namespace test = nstd::test;
using test::Test;

TEST(test_repeat) {
	auto r6 = nstd::tuple::make_repeat<6>(42);
	using R6 = decltype(r6);
	static_assert(std::tuple_size_v<R6> == 6);

	using indices6 = std::make_index_sequence<std::tuple_size_v<R6>>;

	test::static_for([&]<std::size_t i>{
				static_assert(std::same_as<std::tuple_element_t<i, R6>, int>);
				ASSERT_EQ_AT(i, 42, std::get<i>(r6));
			}, indices6{});

	auto rinf = nstd::tuple::make_repeat<nstd::inf{}>(314);
	using RInf = decltype(rinf);
	static_assert(std::tuple_size_v<RInf> == (std::size_t)-1);
	static_assert(nstd::tuple::size_v<RInf> == nstd::inf{});

	using indicesInf = std::index_sequence<0, 1, 2, 34, 55556666, 4000000001ull>;

	test::static_for([&]<std::size_t i>{
				static_assert(std::tuple_size_v<RInf> > i);
				static_assert(i < std::tuple_size_v<RInf>);
				ASSERT_EQ_AT(i, 314, std::get<i>(rinf));
				static_assert(std::same_as<std::tuple_element_t<i, RInf>, int>);
			}, indicesInf{});
};

static const std::string meow{"Meow"};

TEST(test_cat) {
	std::tuple t{1337, 3.14, 42u};
	auto cat = nstd::tuple::cat{std::pair{std::string{"Meow"}, 2.71f}, nstd::tuple::make_repeat<3>(42), 0xDEADBEEF, t};
	using Cat = decltype(cat);
	static_assert(std::tuple_size_v<Cat> == 9);

	using test::ConstRef;
	using test::Constant;
	using test::FindT;
	using test::Mapped;
	using Cases = test::Map<
		Mapped<0, std::string, ConstRef<meow>>,
		Mapped<1, float, Constant<2.71f>>,
		Mapped<2, int, Constant<42>>,
		Mapped<3, int, Constant<42>>,
		Mapped<4, int, Constant<42>>,
		Mapped<5, unsigned int, Constant<0xDEADBEEF>>,
		Mapped<6, int, Constant<1337>>,
		Mapped<7, double, Constant<3.14>>,
		Mapped<8, unsigned int, Constant<42u>>>;

	test::static_for([&]<std::size_t i>{
				using Case = FindT<i, Cases>;

				using ExpectedType = std::tuple_element_t<0, Case>;
				static_assert(std::same_as<ExpectedType, std::tuple_element_t<i, Cat>>);

				auto expectedValue = std::tuple_element_t<1, Case>::value;
				ASSERT_EQ_AT(i, expectedValue, std::get<i>(cat));
			}, Cases{});

	auto [a, b, c] = nstd::tuple::cat{42, std::pair{3.14, std::string{"Hi!"}}};
	ASSERT_EQ(42, a);
	ASSERT_EQ(3.14, b);
	ASSERT_EQ("Hi!", c);

	static_assert(nstd::test::elementwise_same<
			std::tuple<nstd::value_box<1>, nstd::value_box<2>, nstd::value_box<3>>,
			nstd::tuple::map_t<nstd::inc, std::tuple<nstd::value_box<0>, nstd::value_box<1>, nstd::value_box<2>>>>);
};
