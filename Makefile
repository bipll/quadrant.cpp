all: tests

test: directories tests/test_quadrant.cpp tests/test_tuple.cpp tests/main.cpp
	$(CXX) -std=c++23 -fdiagnostics-color=always -ftemplate-backtrace-limit=0 \
		-Iinclude tests/*.cpp -o build/test $(CXXFLAGS)


test_quadrant: directories tests/test_quadrant.cpp
	$(CXX) -std=c++23 -fdiagnostics-color=always -ftemplate-backtrace-limit=0 \
		tests/test_quadrant.cpp tests/main.cpp -Iinclude -o build/test_quadrant $(CXXFLAGS)

test_tuple: directories tests/test_tuple.cpp
	$(CXX) -std=c++23 -fdiagnostics-color=always -ftemplate-backtrace-limit=0 \
		tests/test_tuple.cpp tests/main.cpp -Iinclude -o build/test_tuple $(CXXFLAGS)


directories:
	mkdir -p build

install: test
	cp -R include/quadrant /usr/local/include
