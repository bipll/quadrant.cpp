#ifndef QUADRANT__INF_H_3e1d7b5f6ec06c90da7aa219556e50195f36f94d
#define QUADRANT__INF_H_3e1d7b5f6ec06c90da7aa219556e50195f36f94d

namespace nstd {

struct inf {
	template<class T> constexpr operator T() const noexcept {
		using limits = std::numeric_limits<T>;
		if constexpr(limits::has_infinity) return limits::infinity();
		else return limits::max();
	}
};

}

namespace std {

template<>
struct numeric_limits<nstd::inf> {
	static constexpr bool is_specialized = true;
	static constexpr bool is_signed = false;
	static constexpr bool is_integer = false;
	static constexpr bool is_exact = true;
	static constexpr bool has_infinity = true;	// oh yeah!
	static constexpr bool has_quiet_NaN = false;
	static constexpr bool has_signaling_NaN = false;
	static constexpr float_denorm_style has_denorm = {};
	static constexpr bool has_denorm_loss = false;
	static constexpr float_round_style round_style = round_toward_infinity;
	static constexpr bool is_iec559 = false;
	static constexpr bool is_bounded = true;	// o_0
	static constexpr bool is_modulo = false;
	static constexpr int digits = {};
	static constexpr int digits10 = {};
	static constexpr int max_digits10 = {};
	static constexpr int radix = 2;
	static constexpr int min_exponent = {};
	static constexpr int min_exponent10 = {};
	static constexpr int max_exponent = {};
	static constexpr int max_exponent10 = {};
	static constexpr bool traps = false;
	static constexpr bool tinyness_before = false;

	static constexpr nstd::inf min() noexcept { return {}; }
	static constexpr nstd::inf lowest() noexcept { return {}; }
	static constexpr nstd::inf max() noexcept { return {}; }
	static constexpr nstd::inf epsilon() noexcept { return {}; }
	static constexpr nstd::inf round_error() noexcept { return {}; }
	static constexpr nstd::inf infinity() noexcept { return {}; }
	static constexpr nstd::inf quiet_NaN() noexcept { return {}; }
	static constexpr nstd::inf signaling_NaN() noexcept { return {}; }
	static constexpr nstd::inf denorm_min() noexcept { return {}; }
};

}

namespace nstd {

inline constexpr auto operator<=>(inf, inf) noexcept { return std::strong_ordering::equivalent; }
inline constexpr auto operator<=>(inf, auto) noexcept { return std::strong_ordering::greater; }
inline constexpr auto operator<=>(auto, inf) noexcept { return std::strong_ordering::less; }

template<class Numeric>
requires(std::numeric_limits<Numeric>::has_infinity)
inline constexpr auto operator<=>(Numeric n, inf i) noexcept
{
	if(n == std::numeric_limits<Numeric>::infinity()) return i <=> i;
	else return n <=> i;
}

template<class Numeric>
requires(std::numeric_limits<Numeric>::has_infinity)
inline constexpr auto operator<=>(inf i, Numeric n) noexcept
{
	if(n == std::numeric_limits<Numeric>::infinity()) return i <=> i;
	else return i <=> n;
}

inline constexpr bool operator==(inf, inf) noexcept { return true; }
inline constexpr bool operator==(inf, auto) noexcept { return false; }
inline constexpr bool operator==(auto, inf) noexcept { return false; }

// NB: the following arithmetic definitions ignore the indeterminate results, like inf - inf or inf * 0

inline constexpr inf operator+(inf, inf) noexcept { return {}; }
inline constexpr inf operator+(inf, auto) noexcept { return {}; }
inline constexpr inf operator+(auto, inf) noexcept { return {}; }

inline constexpr inf operator*(inf, inf) noexcept { return {}; }
inline constexpr inf operator*(inf, auto) noexcept { return {}; }
inline constexpr inf operator*(auto, inf) noexcept { return {}; }

template<class T>
requires(!std::convertible_to<T, inf>)
inline constexpr inf operator-(inf, T) noexcept { return {}; }

template<class T>
requires(!std::convertible_to<T, inf>)
inline constexpr inf operator/(inf, T) noexcept { return {}; }

template<class T>
requires(!std::convertible_to<T, inf>)
inline constexpr T operator/(T, inf) noexcept { return {}; }

template<class Numeric>
requires(std::numeric_limits<Numeric>::has_infinity)
inline constexpr bool operator==(Numeric n, inf i) noexcept
{
	if(n == std::numeric_limits<Numeric>::infinity()) return i == i;
	else return n == i;
}

template<class Numeric>
requires(std::numeric_limits<Numeric>::has_infinity)
inline constexpr bool operator==(inf i, Numeric n) noexcept
{
	if(n == std::numeric_limits<Numeric>::infinity()) return i == i;
	else return i == n;
}

}

#endif
