#ifndef QUADRANT__BITS__RUNTIME_SEQUENCE_H_f4549be3d7a79d5f0f55e77bc4fcc071b5b9cee7
#define QUADRANT__BITS__RUNTIME_SEQUENCE_H_f4549be3d7a79d5f0f55e77bc4fcc071b5b9cee7

#include <concepts>
#include <functional>
#include <utility>

namespace nstd::bits {

template<std::size_t maxArity, class T>
struct InitializerList {
	std::initializer_list<T> list;
};

template<std::size_t maxArity, class T>
inline constexpr InitializerList<maxArity, T> initializerList(std::initializer_list<T> list) noexcept {
	return {list};
};

}

namespace std {

template<std::size_t maxArity, class T>
struct tuple_size<nstd::bits::InitializerList<maxArity, T>>
{
	static constexpr std::size_t value = maxArity;
};

template<std::size_t i, std::size_t maxArity, class T>
struct tuple_element<i, nstd::bits::InitializerList<maxArity, T>>
{
	using type = T;
};

template<std::size_t i, std::size_t maxArity, class T>
constexpr T const &get(nstd::bits::InitializerList<maxArity, T> const &list) noexcept {
	return data(list.list)[i];
}

template<std::size_t i, std::size_t maxArity, class T>
constexpr T const &&get(nstd::bits::InitializerList<maxArity, T> const &&list) noexcept {
	return std::move(const_cast<T const &>(data(list.list)[i]));
}

}

namespace nstd::bits {

struct RuntimeSequence {
	template<std::size_t maxArity, class F, class... Args>
	static constexpr decltype(auto) invokeWith(std::size_t size, F &&f, Args... args)
	noexcept(noexcept(lower_bound<maxArity + 1>(size, std::forward<F>(f), args...)))
	{
		return lower_bound<maxArity>(size, std::forward<F>(f), args...);
	}

private:
	template<class F, class... Args> struct nonthrowing: std::true_type {};

	template<class F, class... Args> static constexpr auto nonthrowing_v = nonthrowing<F, Args...>::value;


	template<class F, class Is, class... Args>
	static inline constexpr bool nonthrowing_at = true;


	template<std::size_t maxArity, std::size_t minArity = 0, class F, class... Args>
	static constexpr decltype(auto) lower_bound(std::size_t size, F &&f, Args... args)
	noexcept(nonthrowing_at<F, std::make_index_sequence<maxArity>, Args...>)
	{
		if constexpr(minArity + 1 == maxArity) {
			return std::invoke(std::forward<F>(f), args..., std::make_index_sequence<minArity>{});
		}
		else {
			constexpr std::size_t m = (minArity + maxArity) / 2;
			if(m <= size) return lower_bound<maxArity, m>(size, std::forward<F>(f), args...);
			return lower_bound<m, minArity>(size, std::forward<F>(f), args...);
		}
	}
};

template<class F, class... Args>
requires(std::invocable<F, Args...>)
struct RuntimeSequence::nonthrowing<F, Args...>: std::is_nothrow_invocable<F, Args...> {};

template<class F, std::size_t... is, class... Args>
inline constexpr bool RuntimeSequence::nonthrowing_at<F, std::index_sequence<is...>, Args...> = (nonthrowing_v<F, Args..., std::make_index_sequence<is>> && ...);

}

#endif
