#ifndef QUADRANT__BITS__PRELUDE__COND_H_8276d323da9636d59186eca25143c2256887c1e1
#define QUADRANT__BITS__PRELUDE__COND_H_8276d323da9636d59186eca25143c2256887c1e1

#include "box.h"

namespace nstd {

struct cond {};
template<class... Clauses> using cond_t = invoke_t<cond, Clauses...>;
template<class... Clauses> inline constexpr auto cond_v = invoke_v<cond, Clauses...>;

template<class If, class Then, class... Else> struct force<invoke<cond, If, Then, Else...>>: force<invoke<cond, Else...>> {};

template<true_value If, class Then, class... Else> struct force<invoke<cond, If, Then, Else...>>: force<Then> {};

template<class Else> struct force<invoke<cond, Else>>: force<Else> {};

template<> struct force<invoke<cond>>: force<NSTD_EXCEPTION(cond)> {};


struct static_case {};
template<class... VClauses> using static_case_t = invoke_t<static_case, VClauses...>;
template<class... VClauses> static constexpr auto static_case_v = invoke_v<static_case, VClauses...>;

template<class V, class Then, class... Else> struct force<invoke<static_case, V, V, Then, Else...>>: force<Then> {};

template<class V, class NotV, class Then, class... Else> struct force<invoke<static_case, V, NotV, Then, Else...>>: force<invoke<static_case, V, Else...>> {};

template<class V, class Else> struct force<invoke<static_case, V, Else>>: force<Else> {};

template<class V> struct force<invoke<static_case, V>>: force<NSTD_EXCEPTION(static_case, V)> {};

template<> struct force<invoke<static_case>>: force<NSTD_EXCEPTION(static_case)> {};

}

#endif
