#ifndef QUADRANT__BITS__PRELUDE__BOOL_H_cb60749febfad0ffea1116d649ad2e8b64d96f2b
#define QUADRANT__BITS__PRELUDE__BOOL_H_cb60749febfad0ffea1116d649ad2e8b64d96f2b

#include "box.h"

namespace nstd {

struct conj {};
template<class... Operands> using conj_t = invoke_t<conj, Operands...>;
template<class... Operands> inline constexpr auto conj_v = invoke_v<conj, Operands...>;

template<true_value True, class... Ts> struct force<invoke<conj, True, Ts...>>: force<invoke<conj, Ts...>> {};

template<class False, class... Ts> struct force<invoke<conj, False, Ts...>>: force<False> {};

template<class T> struct force<invoke<conj, T>>: force<T> {};

template<> struct force<invoke<conj>>: true_box {};


struct disj {};
template<class... Operands> using disj_t = invoke_t<disj, Operands...>;
template<class... Operands> inline constexpr auto disj_v = invoke_v<disj, Operands...>;

template<false_value False, class... Ts> struct force<invoke<disj, False, Ts...>>: force<invoke<disj, Ts...>> {};

template<class True, class... Ts> struct force<invoke<disj, True, Ts...>>: force<True> {};

template<class T> struct force<invoke<disj, T>>: force<T> {};

template<> struct force<invoke<disj>>: false_box {};

}

#endif
