#ifndef QUADRANT__BITS__PRELUDE__OP_H_230e347df78a1b4393736fb9060fcecba2939608
#define QUADRANT__BITS__PRELUDE__OP_H_230e347df78a1b4393736fb9060fcecba2939608

#include "box.h"
#include "combinators.h"
#include "let.h"
#include "var.h"

namespace nstd {

struct op {
	struct assoc {};
	struct left {};
	struct right {};
	struct chain {};

	struct arity {};
	struct unary {};
	struct binary {};
	struct complicated {};

	struct unary_form {};

	struct nullary_form {};

	struct fn {};

	template<class OpDef> using assoc_t = find_t<OpDef, assoc, left>;
	template<class OpDef> using arity_t = find_t<OpDef, arity, binary>;
	template<class OpDef> using unary_form_t = find_t<OpDef, unary_form, fun_box<std::type_identity>>;
	template<class OpDef> using nullary_form_t = find_t<OpDef, nullary_form, zero_box>;

	template<class OpDef> using fn_t = find_t<OpDef, fn>;

	template<class OpDef, class V1, class V2> using binary_t = value_box<fn_t<OpDef>{}(force_v<V1>, force_v<V2>)>;
};


struct value_expr {};
template<class OpDef, class... Values> using value_expr_t = invoke_t<value_expr, OpDef, Values...>;
template<class OpDef, class Value, class... Values> inline constexpr auto value_expr_v = invoke_v<value_expr, OpDef, Values...>;

template<class OpDef, class Value1, class Value2, class... Values>
struct force<invoke<value_expr, OpDef, Value1, Value2, Values...>>:
	force<invoke<let,
		  var::b, op::binary_t<OpDef, Value1, Value2>,
		  var::c, invoke<value_expr, OpDef, Value2, Values...>,
		invoke<static_case, op::assoc_t<OpDef>,
			op::left, invoke<value_expr, OpDef, var::b, Values...>,
			op::right, invoke<value_expr, OpDef, Value1, var::c>,
			invoke<conj, var::b, var::c>>>> {};

template<class OpDef, class Value1, class Value2>
struct force<invoke<value_expr, OpDef, Value1, Value2>>:
	force<op::binary_t<OpDef, Value1, Value2>> {};


namespace impl_::op {

template<class UnaryOp, class Value>
inline constexpr decltype(auto) invoke_unary(Value &&value) noexcept {
	if constexpr(requires { { UnaryOp{}(value) }; }) return UnaryOp{}(value);
	else return UnaryOp{}(value, value);
}

template<class UnaryOp, class Value>
inline constexpr decltype(auto) invert_unary(Value &&value) noexcept {
	return UnaryOp{}(Value{}, value);
}

}


template<class OpDef, class Value> struct force<invoke<value_expr, OpDef, Value>>:
	force<invoke<static_case, op::arity_t<OpDef>,
		op::unary, value_box<impl_::op::invoke_unary<op::fn_t<OpDef>>(force_v<Value>)>,
		op::binary, invoke<op::unary_form_t<OpDef>, Value>,
		value_box<impl_::op::invert_unary<op::fn_t<OpDef>>(force_v<Value>)>>> {};

template<class OpDef> struct force<invoke<value_expr, OpDef>>: force<op::nullary_form_t<OpDef>> {};


template<template<class...> class Op, class... MoreFacts> using void_op = bind_t<value_expr, facts<op::fn, Op<void>, MoreFacts...>>;


using plus = void_op<std::plus>;
template<class... Values> using plus_t = invoke_t<plus, Values...>;
template<class... Values> static constexpr auto plus_v = invoke_v<plus, Values...>;

using minus = void_op<std::minus, op::arity, op::complicated>;
template<class... Values> using minus_t = invoke_t<minus, Values...>;
template<class... Values> static constexpr auto minus_v = invoke_v<minus, Values...>;

using multiplies = void_op<std::multiplies, op::nullary_form, one_box>;
template<class... Values> using multiplies_t = invoke_t<multiplies, Values...>;
template<class... Values> static constexpr auto multiplies_v = invoke_v<multiplies, Values...>;

using divides = void_op<std::divides, op::arity, op::complicated, op::nullary_form, one_box>;
template<class... Values> using divides_t = invoke_t<divides, Values...>;
template<class... Values> static constexpr auto divides_v = invoke_v<divides, Values...>;

using modulus = void_op<std::modulus, op::arity, op::complicated, op::nullary_form, one_box>;
template<class... Values> using modulus_t = invoke_t<modulus, Values...>;
template<class... Values> static constexpr auto modulus_v = invoke_v<modulus, Values...>;

using negate = minus;
template<class... Values> using negate_t = invoke_t<negate, Values...>;
template<class... Values> static constexpr auto negate_v = invoke_v<negate, Values...>;

using equal_to = void_op<std::equal_to, op::assoc, op::chain, op::unary_form, konst_t<true_box>, op::nullary_form, true_box>;
template<class... Values> using equal_to_t = invoke_t<equal_to, Values...>;
template<class... Values> static constexpr auto equal_to_v = invoke_v<equal_to, Values...>;

using greater = void_op<std::greater, op::assoc, op::chain, op::unary_form, konst_t<true_box>, op::nullary_form, true_box>;
template<class... Values> using greater_t = invoke_t<greater, Values...>;
template<class... Values> static constexpr auto greater_v = invoke_v<greater, Values...>;

using less = void_op<std::less, op::assoc, op::chain, op::unary_form, konst_t<true_box>, op::nullary_form, true_box>;
template<class... Values> using less_t = invoke_t<less, Values...>;
template<class... Values> static constexpr auto less_v = invoke_v<less, Values...>;

using greater_equal = void_op<std::greater_equal, op::assoc, op::chain, op::unary_form, konst_t<true_box>, op::nullary_form, true_box>;
template<class... Values> using greater_equal_t = invoke_t<greater_equal, Values...>;
template<class... Values> static constexpr auto greater_equal_v = invoke_v<greater_equal, Values...>;

using less_equal = void_op<std::less_equal, op::assoc, op::chain, op::unary_form, konst_t<true_box>, op::nullary_form, true_box>;
template<class... Values> using less_equal_t = invoke_t<less_equal, Values...>;
template<class... Values> static constexpr auto less_equal_v = invoke_v<less_equal, Values...>;

struct logical_and {
	template<class... Values> using type = conj_t<Values...>;
	template<class... Values> static constexpr auto value = conj_v<Values...>;
};
template<class... Values> using logical_and_t = invoke_t<logical_and, Values...>;
template<class... Values> static constexpr auto logical_and_v = invoke_v<logical_and, Values...>;

struct logical_or {
	template<class... Values> using type = disj_t<Values...>;
	template<class... Values> static constexpr auto value = disj_v<Values...>;
};
template<class... Values> using logical_or_t = invoke_t<logical_or, Values...>;
template<class... Values> static constexpr auto logical_or_v = invoke_v<logical_or, Values...>;

using logical_not = void_op<std::logical_not, op::arity, op::unary, op::nullary_form, true_box>;
template<class... Values> using logical_not_t = invoke_t<logical_not, Values...>;
template<class... Values> static constexpr auto logical_not_v = invoke_v<logical_not, Values...>;

using not_equal_to = comp_t<logical_not, equal_to>;
template<class... Values> using not_equal_to_t = invoke_t<not_equal_to, Values...>;
template<class... Values> static constexpr auto not_equal_to_v = invoke_v<not_equal_to, Values...>;

using bit_and = void_op<std::bit_and, op::nullary_form, value_box<-1>>;
template<class... Values> using bit_and_t = invoke_t<bit_and, Values...>;
template<class... Values> static constexpr auto bit_and_v = invoke_v<bit_and, Values...>;

using bit_or = void_op<std::bit_or>;
template<class... Values> using bit_or_t = invoke_t<bit_or, Values...>;
template<class... Values> static constexpr auto bit_or_v = invoke_v<bit_or, Values...>;

using bit_xor = void_op<std::bit_xor>;
template<class... Values> using bit_xor_t = invoke_t<bit_xor, Values...>;
template<class... Values> static constexpr auto bit_xor_v = invoke_v<bit_xor, Values...>;

using bit_not = void_op<std::bit_not, op::arity, op::unary, op::nullary_form, value_box<-1>>;
template<class... Values> using bit_not_t = invoke_t<bit_not, Values...>;
template<class... Values> static constexpr auto bit_not_v = invoke_v<bit_not, Values...>;

using identity = void_op<std::type_identity, op::arity, op::unary>;
template<class... Values> using identity_t = invoke_t<identity, Values...>;
template<class... Values> static constexpr auto identity_v = invoke_v<identity, Values...>;


using inc = bind_t<plus, one_box>;
template<class... Values> using inc_t = invoke_t<inc, Values...>;
template<class... Values> static constexpr auto inc_v = invoke_v<inc, Values...>;

using dec = bind_t<flip_t<minus>, one_box>;
template<class... Values> using dec_t = invoke_t<dec, Values...>;
template<class... Values> static constexpr auto dec_v = invoke_v<dec, Values...>;

using is_zero = bind_t<equal_to, zero_box>;
template<class... Values> using is_zero_t = invoke_t<is_zero, Values...>;
template<class... Values> static constexpr auto is_zero_v = invoke_v<is_zero, Values...>;

using is_positive = bind_t<greater, zero_box>;
template<class... Values> using is_positive_t = invoke_t<is_positive, Values...>;
template<class... Values> static constexpr auto is_positive_v = invoke_v<is_positive, Values...>;

using is_negative = bind_t<less, zero_box>;
template<class... Values> using is_negative_t = invoke_t<is_negative, Values...>;
template<class... Values> static constexpr auto is_negative_v = invoke_v<is_negative, Values...>;

using is_nonpositive = bind_t<less_equal, zero_box>;
template<class... Values> using is_nonpositive_t = invoke_t<is_nonpositive, Values...>;
template<class... Values> static constexpr auto is_nonpositive_v = invoke_v<is_nonpositive, Values...>;

using is_nonnegative = bind_t<greater_equal, zero_box>;
template<class... Values> using is_nonnegative_t = invoke_t<is_nonnegative, Values...>;
template<class... Values> static constexpr auto is_nonnegative_v = invoke_v<is_nonnegative, Values...>;

}

#endif
