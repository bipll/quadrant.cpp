#ifndef QUADRANT__BITS__PRELUDE__VAR_H_b1ab0be4abbaf56e5e75bdc09d47c29f0c69d6aa
#define QUADRANT__BITS__PRELUDE__VAR_H_b1ab0be4abbaf56e5e75bdc09d47c29f0c69d6aa

#include "box.h"

namespace nstd {

namespace var {

struct any {};


template<class...> struct var {};


using a = var<value_box<'a'>>;
using a0 = var<value_box<'a'>, value_box<0>>;
using a1 = var<value_box<'a'>, value_box<1>>;
using a2 = var<value_box<'a'>, value_box<2>>;
using a3 = var<value_box<'a'>, value_box<3>>;
using a3 = var<value_box<'a'>, value_box<3>>;
using a4 = var<value_box<'a'>, value_box<4>>;
using a5 = var<value_box<'a'>, value_box<5>>;
using a6 = var<value_box<'a'>, value_box<6>>;
using a7 = var<value_box<'a'>, value_box<7>>;
using a8 = var<value_box<'a'>, value_box<8>>;
using a9 = var<value_box<'a'>, value_box<9>>;

using b = var<value_box<'b'>>;
using b0 = var<value_box<'b'>, value_box<0>>;
using b1 = var<value_box<'b'>, value_box<1>>;
using b2 = var<value_box<'b'>, value_box<2>>;
using b3 = var<value_box<'b'>, value_box<3>>;
using b4 = var<value_box<'b'>, value_box<4>>;
using b5 = var<value_box<'b'>, value_box<5>>;
using b6 = var<value_box<'b'>, value_box<6>>;
using b7 = var<value_box<'b'>, value_box<7>>;
using b8 = var<value_box<'b'>, value_box<8>>;
using b9 = var<value_box<'b'>, value_box<9>>;

using c = var<value_box<'c'>>;
using c0 = var<value_box<'c'>, value_box<0>>;
using c1 = var<value_box<'c'>, value_box<1>>;
using c2 = var<value_box<'c'>, value_box<2>>;
using c3 = var<value_box<'c'>, value_box<3>>;
using c4 = var<value_box<'c'>, value_box<4>>;
using c5 = var<value_box<'c'>, value_box<5>>;
using c6 = var<value_box<'c'>, value_box<6>>;
using c7 = var<value_box<'c'>, value_box<7>>;
using c8 = var<value_box<'c'>, value_box<8>>;
using c9 = var<value_box<'c'>, value_box<9>>;

using d = var<value_box<'d'>>;
using d0 = var<value_box<'d'>, value_box<0>>;
using d1 = var<value_box<'d'>, value_box<1>>;
using d2 = var<value_box<'d'>, value_box<2>>;
using d3 = var<value_box<'d'>, value_box<3>>;
using d4 = var<value_box<'d'>, value_box<4>>;
using d5 = var<value_box<'d'>, value_box<5>>;
using d6 = var<value_box<'d'>, value_box<6>>;
using d7 = var<value_box<'d'>, value_box<7>>;
using d8 = var<value_box<'d'>, value_box<8>>;
using d9 = var<value_box<'d'>, value_box<9>>;

using e = var<value_box<'e'>>;
using e0 = var<value_box<'e'>, value_box<0>>;
using e1 = var<value_box<'e'>, value_box<1>>;
using e2 = var<value_box<'e'>, value_box<2>>;
using e3 = var<value_box<'e'>, value_box<3>>;
using e4 = var<value_box<'e'>, value_box<4>>;
using e5 = var<value_box<'e'>, value_box<5>>;
using e6 = var<value_box<'e'>, value_box<6>>;
using e7 = var<value_box<'e'>, value_box<7>>;
using e8 = var<value_box<'e'>, value_box<8>>;
using e9 = var<value_box<'e'>, value_box<9>>;

using f = var<value_box<'f'>>;
using f0 = var<value_box<'f'>, value_box<0>>;
using f1 = var<value_box<'f'>, value_box<1>>;
using f2 = var<value_box<'f'>, value_box<2>>;
using f3 = var<value_box<'f'>, value_box<3>>;
using f4 = var<value_box<'f'>, value_box<4>>;
using f5 = var<value_box<'f'>, value_box<5>>;
using f6 = var<value_box<'f'>, value_box<6>>;
using f7 = var<value_box<'f'>, value_box<7>>;
using f8 = var<value_box<'f'>, value_box<8>>;
using f9 = var<value_box<'f'>, value_box<9>>;

using g = var<value_box<'g'>>;
using g0 = var<value_box<'g'>, value_box<0>>;
using g1 = var<value_box<'g'>, value_box<1>>;
using g2 = var<value_box<'g'>, value_box<2>>;
using g3 = var<value_box<'g'>, value_box<3>>;
using g4 = var<value_box<'g'>, value_box<4>>;
using g5 = var<value_box<'g'>, value_box<5>>;
using g6 = var<value_box<'g'>, value_box<6>>;
using g7 = var<value_box<'g'>, value_box<7>>;
using g8 = var<value_box<'g'>, value_box<8>>;
using g9 = var<value_box<'g'>, value_box<9>>;

using h = var<value_box<'h'>>;
using h0 = var<value_box<'h'>, value_box<0>>;
using h1 = var<value_box<'h'>, value_box<1>>;
using h2 = var<value_box<'h'>, value_box<2>>;
using h3 = var<value_box<'h'>, value_box<3>>;
using h4 = var<value_box<'h'>, value_box<4>>;
using h5 = var<value_box<'h'>, value_box<5>>;
using h6 = var<value_box<'h'>, value_box<6>>;
using h7 = var<value_box<'h'>, value_box<7>>;
using h8 = var<value_box<'h'>, value_box<8>>;
using h9 = var<value_box<'h'>, value_box<9>>;

using i = var<value_box<'i'>>;
using i0 = var<value_box<'i'>, value_box<0>>;
using i1 = var<value_box<'i'>, value_box<1>>;
using i2 = var<value_box<'i'>, value_box<2>>;
using i3 = var<value_box<'i'>, value_box<3>>;
using i4 = var<value_box<'i'>, value_box<4>>;
using i5 = var<value_box<'i'>, value_box<5>>;
using i6 = var<value_box<'i'>, value_box<6>>;
using i7 = var<value_box<'i'>, value_box<7>>;
using i8 = var<value_box<'i'>, value_box<8>>;
using i9 = var<value_box<'i'>, value_box<9>>;

using j = var<value_box<'j'>>;
using j0 = var<value_box<'j'>, value_box<0>>;
using j1 = var<value_box<'j'>, value_box<1>>;
using j2 = var<value_box<'j'>, value_box<2>>;
using j3 = var<value_box<'j'>, value_box<3>>;
using j4 = var<value_box<'j'>, value_box<4>>;
using j5 = var<value_box<'j'>, value_box<5>>;
using j6 = var<value_box<'j'>, value_box<6>>;
using j7 = var<value_box<'j'>, value_box<7>>;
using j8 = var<value_box<'j'>, value_box<8>>;
using j9 = var<value_box<'j'>, value_box<9>>;

using k = var<value_box<'k'>>;
using k0 = var<value_box<'k'>, value_box<0>>;
using k1 = var<value_box<'k'>, value_box<1>>;
using k2 = var<value_box<'k'>, value_box<2>>;
using k3 = var<value_box<'k'>, value_box<3>>;
using k4 = var<value_box<'k'>, value_box<4>>;
using k5 = var<value_box<'k'>, value_box<5>>;
using k6 = var<value_box<'k'>, value_box<6>>;
using k7 = var<value_box<'k'>, value_box<7>>;
using k8 = var<value_box<'k'>, value_box<8>>;
using k9 = var<value_box<'k'>, value_box<9>>;

using l = var<value_box<'l'>>;
using l0 = var<value_box<'l'>, value_box<0>>;
using l1 = var<value_box<'l'>, value_box<1>>;
using l2 = var<value_box<'l'>, value_box<2>>;
using l3 = var<value_box<'l'>, value_box<3>>;
using l4 = var<value_box<'l'>, value_box<4>>;
using l5 = var<value_box<'l'>, value_box<5>>;
using l6 = var<value_box<'l'>, value_box<6>>;
using l7 = var<value_box<'l'>, value_box<7>>;
using l8 = var<value_box<'l'>, value_box<8>>;
using l9 = var<value_box<'l'>, value_box<9>>;

using m = var<value_box<'m'>>;
using m0 = var<value_box<'m'>, value_box<0>>;
using m1 = var<value_box<'m'>, value_box<1>>;
using m2 = var<value_box<'m'>, value_box<2>>;
using m3 = var<value_box<'m'>, value_box<3>>;
using m4 = var<value_box<'m'>, value_box<4>>;
using m5 = var<value_box<'m'>, value_box<5>>;
using m6 = var<value_box<'m'>, value_box<6>>;
using m7 = var<value_box<'m'>, value_box<7>>;
using m8 = var<value_box<'m'>, value_box<8>>;
using m9 = var<value_box<'m'>, value_box<9>>;

using n = var<value_box<'n'>>;
using n0 = var<value_box<'n'>, value_box<0>>;
using n1 = var<value_box<'n'>, value_box<1>>;
using n2 = var<value_box<'n'>, value_box<2>>;
using n3 = var<value_box<'n'>, value_box<3>>;
using n4 = var<value_box<'n'>, value_box<4>>;
using n5 = var<value_box<'n'>, value_box<5>>;
using n6 = var<value_box<'n'>, value_box<6>>;
using n7 = var<value_box<'n'>, value_box<7>>;
using n8 = var<value_box<'n'>, value_box<8>>;
using n9 = var<value_box<'n'>, value_box<9>>;

using o = var<value_box<'o'>>;
using o0 = var<value_box<'o'>, value_box<0>>;
using o1 = var<value_box<'o'>, value_box<1>>;
using o2 = var<value_box<'o'>, value_box<2>>;
using o3 = var<value_box<'o'>, value_box<3>>;
using o4 = var<value_box<'o'>, value_box<4>>;
using o5 = var<value_box<'o'>, value_box<5>>;
using o6 = var<value_box<'o'>, value_box<6>>;
using o7 = var<value_box<'o'>, value_box<7>>;
using o8 = var<value_box<'o'>, value_box<8>>;
using o9 = var<value_box<'o'>, value_box<9>>;

using p = var<value_box<'p'>>;
using p0 = var<value_box<'p'>, value_box<0>>;
using p1 = var<value_box<'p'>, value_box<1>>;
using p2 = var<value_box<'p'>, value_box<2>>;
using p3 = var<value_box<'p'>, value_box<3>>;
using p4 = var<value_box<'p'>, value_box<4>>;
using p5 = var<value_box<'p'>, value_box<5>>;
using p6 = var<value_box<'p'>, value_box<6>>;
using p7 = var<value_box<'p'>, value_box<7>>;
using p8 = var<value_box<'p'>, value_box<8>>;
using p9 = var<value_box<'p'>, value_box<9>>;

using q = var<value_box<'q'>>;
using q0 = var<value_box<'q'>, value_box<0>>;
using q1 = var<value_box<'q'>, value_box<1>>;
using q2 = var<value_box<'q'>, value_box<2>>;
using q3 = var<value_box<'q'>, value_box<3>>;
using q4 = var<value_box<'q'>, value_box<4>>;
using q5 = var<value_box<'q'>, value_box<5>>;
using q6 = var<value_box<'q'>, value_box<6>>;
using q7 = var<value_box<'q'>, value_box<7>>;
using q8 = var<value_box<'q'>, value_box<8>>;
using q9 = var<value_box<'q'>, value_box<9>>;

using r = var<value_box<'r'>>;
using r0 = var<value_box<'r'>, value_box<0>>;
using r1 = var<value_box<'r'>, value_box<1>>;
using r2 = var<value_box<'r'>, value_box<2>>;
using r3 = var<value_box<'r'>, value_box<3>>;
using r4 = var<value_box<'r'>, value_box<4>>;
using r5 = var<value_box<'r'>, value_box<5>>;
using r6 = var<value_box<'r'>, value_box<6>>;
using r7 = var<value_box<'r'>, value_box<7>>;
using r8 = var<value_box<'r'>, value_box<8>>;
using r9 = var<value_box<'r'>, value_box<9>>;

using s = var<value_box<'s'>>;
using s0 = var<value_box<'s'>, value_box<0>>;
using s1 = var<value_box<'s'>, value_box<1>>;
using s2 = var<value_box<'s'>, value_box<2>>;
using s3 = var<value_box<'s'>, value_box<3>>;
using s4 = var<value_box<'s'>, value_box<4>>;
using s5 = var<value_box<'s'>, value_box<5>>;
using s6 = var<value_box<'s'>, value_box<6>>;
using s7 = var<value_box<'s'>, value_box<7>>;
using s8 = var<value_box<'s'>, value_box<8>>;
using s9 = var<value_box<'s'>, value_box<9>>;

using t = var<value_box<'t'>>;
using t0 = var<value_box<'t'>, value_box<0>>;
using t1 = var<value_box<'t'>, value_box<1>>;
using t2 = var<value_box<'t'>, value_box<2>>;
using t3 = var<value_box<'t'>, value_box<3>>;
using t4 = var<value_box<'t'>, value_box<4>>;
using t5 = var<value_box<'t'>, value_box<5>>;
using t6 = var<value_box<'t'>, value_box<6>>;
using t7 = var<value_box<'t'>, value_box<7>>;
using t8 = var<value_box<'t'>, value_box<8>>;
using t9 = var<value_box<'t'>, value_box<9>>;

using u = var<value_box<'u'>>;
using u0 = var<value_box<'u'>, value_box<0>>;
using u1 = var<value_box<'u'>, value_box<1>>;
using u2 = var<value_box<'u'>, value_box<2>>;
using u3 = var<value_box<'u'>, value_box<3>>;
using u4 = var<value_box<'u'>, value_box<4>>;
using u5 = var<value_box<'u'>, value_box<5>>;
using u6 = var<value_box<'u'>, value_box<6>>;
using u7 = var<value_box<'u'>, value_box<7>>;
using u8 = var<value_box<'u'>, value_box<8>>;
using u9 = var<value_box<'u'>, value_box<9>>;

using v = var<value_box<'v'>>;
using v0 = var<value_box<'v'>, value_box<0>>;
using v1 = var<value_box<'v'>, value_box<1>>;
using v2 = var<value_box<'v'>, value_box<2>>;
using v3 = var<value_box<'v'>, value_box<3>>;
using v4 = var<value_box<'v'>, value_box<4>>;
using v5 = var<value_box<'v'>, value_box<5>>;
using v6 = var<value_box<'v'>, value_box<6>>;
using v7 = var<value_box<'v'>, value_box<7>>;
using v8 = var<value_box<'v'>, value_box<8>>;
using v9 = var<value_box<'v'>, value_box<9>>;

using w = var<value_box<'w'>>;
using w0 = var<value_box<'w'>, value_box<0>>;
using w1 = var<value_box<'w'>, value_box<1>>;
using w2 = var<value_box<'w'>, value_box<2>>;
using w3 = var<value_box<'w'>, value_box<3>>;
using w4 = var<value_box<'w'>, value_box<4>>;
using w5 = var<value_box<'w'>, value_box<5>>;
using w6 = var<value_box<'w'>, value_box<6>>;
using w7 = var<value_box<'w'>, value_box<7>>;
using w8 = var<value_box<'w'>, value_box<8>>;
using w9 = var<value_box<'w'>, value_box<9>>;

using x = var<value_box<'x'>>;
using x0 = var<value_box<'x'>, value_box<0>>;
using x1 = var<value_box<'x'>, value_box<1>>;
using x2 = var<value_box<'x'>, value_box<2>>;
using x3 = var<value_box<'x'>, value_box<3>>;
using x4 = var<value_box<'x'>, value_box<4>>;
using x5 = var<value_box<'x'>, value_box<5>>;
using x6 = var<value_box<'x'>, value_box<6>>;
using x7 = var<value_box<'x'>, value_box<7>>;
using x8 = var<value_box<'x'>, value_box<8>>;
using x9 = var<value_box<'x'>, value_box<9>>;

using y = var<value_box<'y'>>;
using y0 = var<value_box<'y'>, value_box<0>>;
using y1 = var<value_box<'y'>, value_box<1>>;
using y2 = var<value_box<'y'>, value_box<2>>;
using y3 = var<value_box<'y'>, value_box<3>>;
using y4 = var<value_box<'y'>, value_box<4>>;
using y5 = var<value_box<'y'>, value_box<5>>;
using y6 = var<value_box<'y'>, value_box<6>>;
using y7 = var<value_box<'y'>, value_box<7>>;
using y8 = var<value_box<'y'>, value_box<8>>;
using y9 = var<value_box<'y'>, value_box<9>>;

using z = var<value_box<'z'>>;
using z0 = var<value_box<'z'>, value_box<0>>;
using z1 = var<value_box<'z'>, value_box<1>>;
using z2 = var<value_box<'z'>, value_box<2>>;
using z3 = var<value_box<'z'>, value_box<3>>;
using z4 = var<value_box<'z'>, value_box<4>>;
using z5 = var<value_box<'z'>, value_box<5>>;
using z6 = var<value_box<'z'>, value_box<6>>;
using z7 = var<value_box<'z'>, value_box<7>>;
using z8 = var<value_box<'z'>, value_box<8>>;
using z9 = var<value_box<'z'>, value_box<9>>;


template<class Symbol> struct seq {};


struct obj {};

}

template<template<class...> class F, class... Args>
struct force<invoke<is_invocation, F<Args...>, var::any>>: true_box {};

}

#endif
