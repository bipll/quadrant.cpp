#ifndef QUADRANT__BITS__PRELUDE__LET_H_6d09a5066d1690bc82370610404657694c2b062c
#define QUADRANT__BITS__PRELUDE__LET_H_6d09a5066d1690bc82370610404657694c2b062c

#include "box.h"
#include "var.h"

namespace nstd {

namespace impl_::let {

template<class Frame, class... BindingsExpr> struct collect_bindings {};

template<class Frame, class... BindingsExpr> struct collect_form {};

template<class Var> struct current_seq {};

template<class Frame, class Expr> struct execute {};
template<class Frame, class Expr> using execute_t = force_t<execute<Frame, Expr>>;

template<class... Expanded> struct list_expansion {};

template<template<class...> class F, class ListExpansion, class... Args> struct form_expansion {};

}

struct let {};
template<class... BindingsExpr> using let_t = invoke_t<let, BindingsExpr...>;
template<class... BindingsExpr> static constexpr auto let_v = invoke_v<let, BindingsExpr...>;

template<class... BindingsExpr> struct force<invoke<let, BindingsExpr...>>: force<impl_::let::collect_bindings<facts<>, BindingsExpr...>> {};


template<class Frame, class Pattern, class Value, class... BindingsExpr>
struct force<impl_::let::collect_bindings<Frame, Pattern, Value, BindingsExpr...>>:
	force<cond,
		same_as<Pattern, var::any>, impl_::let::collect_bindings<Frame, BindingsExpr...>,
		same_as<Pattern, Value>, impl_::let::collect_bindings<Frame, BindingsExpr...>,
		is_call<Pattern, invoke>, impl_::let::collect_form<Frame, Pattern, Value, BindingsExpr...>,
		is_call<Pattern, var::var>, invoke<cond,
			invoke<indistinguishable, delay<Value>, invoke<find, Frame, Pattern, delay<Value>>>,
				impl_::let::collect_bindings<emplace_t<Frame, Pattern, delay<Value>>, BindingsExpr...>,
			NSTD_EXCEPTION(Frame, Pattern, Value, find_t<Frame, Pattern>)>,
		NSTD_EXCEPTION(Frame, Pattern, Value)> {};

template<class Frame, class Expr> struct force<impl_::let::collect_bindings<Frame, Expr>>: force<impl_::let::execute<Frame, Expr>> {};


template<class Frame, class Expr>
struct force<impl_::let::execute<Frame, Expr>>: force<Expr> {};

template<class Frame, template<class...> class F, class... Args>
struct force<impl_::let::execute<Frame, F<Args...>>>:
	force<impl_::let::execute<Frame, impl_::let::form_expansion<F, impl_::let::list_expansion<>, Args...>>> {};

template<class Frame, template<class...> class F, class... Expanded, class Arg, class... Args>
struct force<impl_::let::execute<Frame, impl_::let::form_expansion<F, impl_::let::list_expansion<Expanded...>, Arg, Args...>>>:
	force<impl_::let::execute<Frame,
		impl_::let::form_expansion<F,
			impl_::let::list_expansion<Expanded..., impl_::let::execute_t<Frame, Arg>>,
			Args...>>>
{};

template<class Frame, template<class...> class F, class... Expanded, class Symbol, class... Args>
struct force<impl_::let::execute<Frame, impl_::let::form_expansion<F, impl_::let::list_expansion<Expanded...>, var::seq<Symbol>, Args...>>>:
	force<impl_::let::execute<Frame, impl_::let::form_expansion<F,
		impl_::let::list_expansion<Expanded...>,
		impl_::let::list_expansion<find_t<Frame, Symbol>>,
		Args...>>>
{};

template<class Frame, template<class...> class F, class... Expanded, class... Seq, class... Args>
struct force<impl_::let::execute<Frame, impl_::let::form_expansion<F,
		impl_::let::list_expansion<Expanded...>,
		impl_::let::list_expansion<std::tuple<Seq...>>,
		Args...>>>:
	force<impl_::let::execute<Frame, impl_::let::form_expansion<F,
		impl_::let::list_expansion<Expanded...>,
		Seq...,
		Args...>>> {};

template<class Frame, template<class...> class F, class... Expanded>
struct force<impl_::let::execute<Frame, impl_::let::form_expansion<F, impl_::let::list_expansion<Expanded...>>>>:
	force<call<F, strict<Expanded>...>> {};

template<class Frame, class... Symbol>
struct force<impl_::let::execute<Frame, var::var<Symbol...>>>:
	force<impl_::let::execute<Frame, find_t<Frame, var::var<Symbol...>>>> {};

template<class Frame, class Symbol>
struct force<impl_::let::execute<Frame, var::seq<Symbol>>>:
	force<invoke<find, Frame, Symbol>> {};


template<class Frame, class FPat, class... ArgPats, template<class...> class F, class... Args, class... BindingsExpr>
requires(!same_fun_v<F, invoke>)
struct force<impl_::let::collect_form<Frame, invoke<FPat, ArgPats...>, F<Args...>, BindingsExpr...>>:
	force<impl_::let::collect_form<Frame, invoke<FPat, ArgPats...>, invoke<fun_box<F>, Args...>, BindingsExpr...>> {};

template<class Frame, class FPat, class... ArgPats, class F, class... Args, class... BindingsExpr>
requires(!is_call_v<FPat, var::seq>)
struct force<impl_::let::collect_form<Frame, invoke<FPat, ArgPats...>, invoke<F, Args...>, BindingsExpr...>>:
	force<impl_::let::collect_form<Frame, invoke<ArgPats...>, invoke<Args...>, FPat, F, BindingsExpr...>> {};

template<class Frame, class Var, class... ArgPats, class... Args, class... BindingsExpr>
requires(sizeof...(ArgPats) <= sizeof...(Args))
struct force<impl_::let::collect_form<Frame, invoke<var::seq<Var>, ArgPats...>, invoke<Args...>, BindingsExpr...>>:
	force<impl_::let::collect_form<Frame, invoke<impl_::let::current_seq<Var>, ArgPats...>, invoke<std::tuple<>, Args...>, BindingsExpr...>> {};

template<class Frame, class Var, class... ArgPats, class... Collected, class Next, class... Args, class... BindingsExpr>
requires(sizeof...(ArgPats) <= sizeof...(Args))
struct force<impl_::let::collect_form<Frame,
		invoke<impl_::let::current_seq<Var>, ArgPats...>,
		invoke<std::tuple<Collected...>, Next, Args...>,
		BindingsExpr...>>:
	force<impl_::let::collect_form<Frame,
		invoke<impl_::let::current_seq<Var>, ArgPats...>,
		invoke<std::tuple<Collected..., Next>, Args...>,
		BindingsExpr...>> {};

template<class Frame, class Var, class... ArgPats, class Tuple, class... Args, class... BindingsExpr>
struct force<impl_::let::collect_form<Frame, invoke<impl_::let::current_seq<Var>, ArgPats...>, invoke<Tuple, Args...>, BindingsExpr...>>:
	force<impl_::let::collect_form<Frame, invoke<ArgPats...>, invoke<Args...>, Var, Tuple, BindingsExpr...>> {};

template<class Frame, class... ExcessArgs, class... BindingsExpr>
struct force<impl_::let::collect_form<Frame, invoke<>, invoke<ExcessArgs...>, BindingsExpr...>>:
	force<impl_::let::collect_bindings<Frame, BindingsExpr...>> {};

}

#endif
