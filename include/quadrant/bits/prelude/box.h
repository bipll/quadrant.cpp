#ifndef QUADRANT__BITS__PRELUDE__BOX_H_93bce06506dfecb51464163df5506c9e3d9503bb
#define QUADRANT__BITS__PRELUDE__BOX_H_93bce06506dfecb51464163df5506c9e3d9503bb

namespace nstd {

template<auto v> struct value_box {
	static constexpr auto value = v;
};


using zero_value = value_box<0>;
using one_value = value_box<1>;


template<class T1, class T2> using same_as = value_box<std::same_as<T1, T2>>;


template<class T> struct type_box {
	using strict_type = T;
};


template<class T>
concept boxed_type = requires { requires(!std::same_as<T, typename T::type>); };


template<boxed_type T> using convert = type_box<typename T::type>;


template<class T>
concept forced_type = requires { typename T::strict_type; };


template<class...> struct force;
template<class... Ts> using force_t = typename force<Ts...>::strict_type;
template<class... Ts> static constexpr auto force_v = force_t<Ts...>::value;

template<class T> struct force<T>: type_box<T> {};

template<forced_type T> struct force<T>: force<typename T::strict_type> {};


using true_box = force<value_box<true>>;
using false_box = force<value_box<false>>;
using zero_box = force<value_box<0>>;
using one_box = force<value_box<1>>;


template<auto line, class...> struct Exception;
#define NSTD_EXCEPTION(...) ::nstd::Exception<__LINE__, __VA_ARGS__>


template<class T> struct delay {};
template<class T> using delay_t = force_t<delay<T>>;
template<class T> static constexpr auto delay_v = force_v<delay<T>>;

template<class T> struct force<delay<T>>: type_box<T> {};


template<class T> struct formal_arg: type_box<T> {};
template<class T> using formal_arg_t = typename formal_arg<T>::strict_type;
template<class T> inline constexpr auto formal_arg_v = formal_arg_t<T>::value;

template<class T> struct strict {};
template<class T> struct formal_arg<strict<T>>: force<T> {};
template<class T> struct force<strict<T>>: force<T> {};


template<template<class...> class F, class... Args> struct call {};
template<template<class...> class F, class... Args> struct force<call<F, Args...>>: force<F<formal_arg_t<Args>...>> {};
template<template<class...> class F, class... Args> using call_t = force_t<call<F, Args...>>;
template<template<class...> class F, class... Args> inline constexpr auto call_v = force_v<call<F, Args...>>;


template<class... FArgs> struct invoke {};
template<class F, class... Args> struct force<invoke<F, Args...>>: force<call<F::template type, Args...>> {};
template<class F, class... Args> using invoke_t = force_t<invoke<F, Args...>>;
template<class F, class... Args> inline constexpr auto invoke_v = force_v<invoke<F, Args...>>;


template<class F, class... Ts> struct force<F, Ts...>: force<invoke<F, Ts...>> {};


struct indistinguishable {};
template<class T1, class T2> using indistinguishable_t = invoke_t<indistinguishable, T1, T2>;
template<class T1, class T2> static constexpr auto indistinguishable_v = invoke_v<indistinguishable, T1, T2>;

template<class T1, class T2> struct force<invoke<indistinguishable, T1, T2>>: force<call<same_as, strict<T1>, strict<T2>>> {};


template<template<class...> class F> struct fun_box {
	template<class... Args> using type = call_t<F, Args...>;
	template<class... Args> static constexpr auto value = call_v<F, Args...>;
};

template<class T>
concept true_value = bool(force_v<T>);

template<class T>
concept false_value = !true_value<T>;


template<class T1, class T2> using same_as_t = call_t<same_as, T1, T2>;
template<class T1, class T2> static constexpr auto same_as_v = call_v<same_as, T1, T2>;


template<template<class...> class F, template<class...> class G> struct same_fun: false_box {};
template<template<class...> class F, template<class...> class G> using same_fun_t = force_t<same_fun<F, G>>;
template<template<class...> class F, template<class...> class G> static constexpr auto same_fun_v = force_v<same_fun<F, G>>;

template<template<class...> class F> struct same_fun<F, F>: true_box {};


template<template<class...> class F, template<class...> class G> struct force<invoke<indistinguishable, fun_box<F>, fun_box<G>>>:
	force<same_fun<F, G>> {};


template<class T, template<class...> class F> struct is_call {};
template<class T, template<class...> class F> using is_call_t = force_t<is_call<T, F>>;
template<class T, template<class...> class F> static constexpr auto is_call_v = force_v<is_call<T, F>>;

template<class T, template<class...> class F> struct force<is_call<T, F>>: false_box {};

template<class... Args, template<class...> class F> struct force<is_call<F<Args...>, F>>: true_box {};


struct is_invocation {};
template<class T, class F> using is_invocation_t = invoke_t<is_invocation, T, F>;
template<class T, class F> static constexpr auto is_invocation_v = invoke_v<is_invocation, T, F>;

template<class T, class F> struct force<invoke<is_invocation, T, F>>: false_box {};

template<class T, class F>
requires(is_call_v<T, F::template type>)
struct force<invoke<is_invocation, T, F>>: true_box {};


template<class T> struct decompose_call {};
template<class T> using called_fun_t = typename decompose_call<T>::f;
template<class T> using args_t = typename decompose_call<T>::args;

template<template<class...> class F, class... Args> struct decompose_call<F<Args...>> {
	using f = fun_box<F>;
	using args = std::tuple<Args...>;
};


struct apply {};
template<class F, class T> using apply_t = invoke_t<apply, F, T>;
template<class F, class T> static constexpr auto apply_v = invoke_v<apply, F, T>;

template<class F, class T> struct force<invoke<apply, F, T>>: force<invoke<apply, F, args_t<T>>> {};

template<class F, class... Args> struct force<invoke<apply, F, std::tuple<Args...>>>: force<invoke<F, Args...>> {};

}

#endif
