#ifndef QUADRANT__BITS__PRELUDE__TYPE_FUNCTION_H_e54fdeb7a6528c38ef7ad9d8862680f4814f70f1
#define QUADRANT__BITS__PRELUDE__TYPE_FUNCTION_H_e54fdeb7a6528c38ef7ad9d8862680f4814f70f1

#include "box.h"

namespace nstd {

template<template<auto, class...> class F> struct type_function_0 {};

template<template<auto, class...> class F, class V0, class... Ts>
struct force<invoke<type_function_0<F>, V0, Ts...>>:
	force<F<force_v<V0>, formal_arg_t<Ts>...>> {};


template<template<class, auto, class...> class F> struct type_function_1 {};

template<template<class, auto, class...> class F, class T0, class V1, class... Ts>
struct force<invoke<type_function_1<F>, T0, V1, Ts...>>:
	force<F<formal_arg_t<T0>, force_v<V1>, formal_arg_t<Ts>...>> {};


template<template<class, class, auto, class...> class F> struct type_function_2 {};

template<template<class, class, auto, class...> class F, class T0, class T1, class V2, class... Ts>
struct force<invoke<type_function_2<F>, T0, T1, V2, Ts...>>:
	force<F<formal_arg_t<T0>, formal_arg_t<T1>, force_v<V2>, formal_arg_t<Ts>...>> {};


template<template<class, class, class, auto, class...> class F> struct type_function_3 {};

template<template<class, class, class, auto, class...> class F, class T0, class T1, class T2, class V3, class... Ts>
struct force<invoke<type_function_3<F>, T0, T1, T2, V3, Ts...>>:
	force<F<formal_arg_t<T0>, formal_arg_t<T1>, formal_arg_t<T2>, force_v<V3>, formal_arg_t<Ts>...>> {};


template<template<auto, auto, class...> class F> struct type_function_01 {};

template<template<auto, auto, class...> class F, class V0, class V1, class... Ts>
struct force<invoke<type_function_01<F>, V0, V1, Ts...>>:
	force<F<force_v<V0>, force_v<V1>, formal_arg_t<Ts>...>> {};


template<template<class, auto, auto, class...> class F> struct type_function_12 {};

template<template<class, auto, auto, class...> class F, class T0, class V1, class V2, class... Ts>
struct force<invoke<type_function_12<F>, T0, V1, V2, Ts...>>:
	force<F<formal_arg_t<T0>, force_v<V1>, force_v<V2>, formal_arg_t<Ts>...>> {};


template<template<class, class, auto, auto, class...> class F> struct type_function_23 {};

template<template<class, class, auto, auto, class...> class F, class T0, class T1, class V2, class V3, class... Ts>
struct force<invoke<type_function_23<F>, T0, T1, V2, V3, Ts...>>:
	force<F<formal_arg_t<T0>, formal_arg_t<T1>, force_v<V2>, force_v<V3>, formal_arg_t<Ts>...>> {};


template<template<class, class, class, auto, auto, class...> class F> struct type_function_34 {};

template<template<class, class, class, auto, auto, class...> class F, class T0, class T1, class T2, class V3, class V4, class... Ts>
struct force<invoke<type_function_34<F>, T0, T1, T2, V3, V4, Ts...>>:
	force<F<formal_arg_t<T0>, formal_arg_t<T1>, formal_arg_t<T2>, force_v<V3>, force_v<V4>, formal_arg_t<Ts>...>> {};



template<template<auto, class, auto, class...> class F> struct type_function_02 {};

template<template<auto, class, auto, class...> class F, class V0, class T1, class V2, class... Ts>
struct force<invoke<type_function_02<F>, V0, T1, V2, Ts...>>:
	force<F<force_v<V0>, formal_arg_t<T1>, force_v<V2>, formal_arg_t<Ts>...>> {};


template<template<class, auto, class, auto, class...> class F> struct type_function_13 {};

template<template<class, auto, class, auto, class...> class F, class T0, class V1, class T2, class V3, class... Ts>
struct force<invoke<type_function_13<F>, T0, V1, T2, V3, Ts...>>:
	force<F<formal_arg_t<T0>, force_v<V1>, formal_arg_t<T2>, force_v<V3>, formal_arg_t<Ts>...>> {};


template<template<class, class, auto, class, auto, class...> class F> struct type_function_24 {};

template<template<class, class, auto, class, auto, class...> class F, class T0, class T1, class V2, class T3, class V4, class... Ts>
struct force<invoke<type_function_24<F>, T0, T1, V2, T3, V4, Ts...>>:
	force<F<formal_arg_t<T0>, formal_arg_t<T1>, force_v<V2>, formal_arg_t<T3>, force_v<V4>, formal_arg_t<Ts>...>> {};


template<template<class, class, class, auto, class, auto, class...> class F> struct type_function_35 {};

template<template<class, class, class, auto, class, auto, class...> class F,
	class T0, class T1, class T2, class V3, class T4, class V5, class... Ts>
struct force<invoke<type_function_35<F>, T0, T1, T2, V3, T4, V5, Ts...>>:
	force<F<formal_arg_t<T0>, formal_arg_t<T1>, formal_arg_t<T2>, force_v<V3>, formal_arg_t<T4>, force_v<V5>, formal_arg_t<Ts>...>> {};

}

#endif
