#ifndef QUADRANT__BITS__PRELUDE__COMBINATORS_H_125ed07eab06a886fb0e9419f6da26155a2c9666
#define QUADRANT__BITS__PRELUDE__COMBINATORS_H_125ed07eab06a886fb0e9419f6da26155a2c9666

#include "box.h"

namespace nstd {

namespace impl_::combinators {

template<class RV> struct konst {
	template<class...> using type = force_t<RV>;
	template<class...> static constexpr auto value = force_v<RV>;
};


template<class F> struct flip {
	template<class A, class B, class... Ts> using type = invoke_t<F, B, A, Ts...>;
	template<class A, class B, class... Ts> static constexpr auto type_v = invoke_v<F, A, B, Ts...>;
};


template<class F, class... Ts> struct bind {
	template<class... Us> using type = invoke_t<F, Ts..., Us...>;
	template<class... Us> static constexpr auto value = invoke_v<F, Ts..., Us...>;
};


template<class F, class... Gs> struct comp {
	template<class... Ts> using type = invoke_t<F, invoke_t<Gs, Ts...>...>;
	template<class... Ts> static constexpr auto value = call_v<type, Ts...>;
};

}


struct konst {};
template<class RV> using konst_t = invoke_t<konst, RV>;
template<class RV> struct force<invoke<konst, RV>>: type_box<impl_::combinators::konst<RV>> {};


struct flip {};
template<class F> using flip_t = force_t<invoke<flip, F>>;
template<class F> struct force<invoke<flip, F>>: type_box<impl_::combinators::flip<F>> {};


struct bind {};
template<class F, class... Ts> using bind_t = force_t<invoke<bind, F, Ts...>>;
template<class F, class... Ts> struct force<invoke<bind, F, Ts...>>: type_box<impl_::combinators::bind<F, Ts...>> {};


struct comp {};
template<class F, class... Gs> using comp_t = force_t<invoke<comp, F, Gs...>>;
template<class F, class... Gs> struct force<invoke<comp, F, Gs...>>: type_box<impl_::combinators::comp<F, Gs...>> {};

}

#endif
