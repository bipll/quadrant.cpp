#ifndef QUADRANT__BITS__PRELUDE__IMPERATIVE_H_c25d62659f5c1dbcaf0bad15dc1755d29c79c7b0
#define QUADRANT__BITS__PRELUDE__IMPERATIVE_H_c25d62659f5c1dbcaf0bad15dc1755d29c79c7b0

#include "box.h"
#include "var.h"

namespace nstd {

struct with {};
template<class Obj, class... Forms> using with_t = invoke_t<with, Obj, Forms...>;
template<class Obj, class... Forms> static constexpr auto with_v = invoke_v<with, Obj, Forms...>;

template<class Obj, class F, class... Forms>
struct force<invoke<with, Obj, F, Forms...>>:
	force<invoke<with, invoke_t<F, Obj>, Forms...>> {};

template<class Obj, class Form, class... Args, class... Forms>
struct force<invoke<with, Obj, invoke<Form, Args...>, Forms...>>:
	force<invoke<with, invoke_t<Form, Obj, Args...>, Forms...>> {};

template<class Obj>
struct force<invoke<with, Obj>>: force<Obj> {};

template<class... UnknownArgs>
struct force<invoke<with, UnknownArgs...>>: force<NSTD_EXCEPTION(with, UnknownArgs...)> {};


namespace impl_::script {

template<class... FArgs> struct invoke {};

}


struct script {};
template<class Obj, class... Forms> using script_t = invoke_t<script, Obj, Forms...>;
template<class Obj, class... Forms> static constexpr auto script_v = invoke_v<script, Obj, Forms...>;

template<class Obj, class F, class... Forms>
struct force<invoke<script, Obj, F, Forms...>>:
	force<invoke<script, invoke_t<F, Obj>, Forms...>> {};

template<class Obj, class Form, class... Args, class... Forms>
struct force<invoke<script, Obj, invoke<Form, Args...>, Forms...>>:
	force<invoke<script, Obj, impl_::script::invoke<Form>, invoke<Args...>, Forms...>> {};

template<class Obj, class Form, class... Args, class Arg, class... MoreArgs, class... Forms>
struct force<invoke<script, Obj, impl_::script::invoke<Form, Args...>, invoke<Arg, MoreArgs...>, Forms...>>:
	force<invoke<cond,
		same_as<Arg, var::obj>, invoke<script, Obj, impl_::script::invoke<Form, Args..., Obj>, invoke<MoreArgs...>, Forms...>,
		invoke<script, Obj, impl_::script::invoke<Form, Args..., Arg>, invoke<MoreArgs...>, Forms...>>> {};

template<class Obj, class Form, class... Args, class... Forms>
struct force<invoke<script, Obj, impl_::script::invoke<Form, Args...>, invoke<>, Forms...>>:
	force<invoke<script, invoke_t<Form, Args...>, Forms...>> {};

template<class Obj>
struct force<invoke<script, Obj>>: force<Obj> {};

template<class... UnknownArgs>
struct force<invoke<script, UnknownArgs...>>: force<NSTD_EXCEPTION(script, UnknownArgs...)> {};

}

#endif
