#ifndef QUADRANT__BITS__PRELUDE__FACTS_H_b161ee815eeb1a35a7b3bdba99d192a2c4120228
#define QUADRANT__BITS__PRELUDE__FACTS_H_b161ee815eeb1a35a7b3bdba99d192a2c4120228

#include "box.h"

namespace nstd {

template<class... Items> struct facts {};


struct find {};
template<class Facts, class Key, class Default = NSTD_EXCEPTION(Facts, Key)> using find_t = invoke_t<find, Facts, Key, Default>;
template<class Facts, class Key, class Default = NSTD_EXCEPTION(Facts, Key)> static constexpr auto find_v = invoke_v<find, Facts, Key, Default>;

template<class K, class V, class... OtherFacts, class Key, class Default> struct force<invoke<find, facts<K, V, OtherFacts...>, Key, Default>>:
	force<invoke<cond, same_as<K, Key>, V, invoke<find, facts<OtherFacts...>, Key, Default>>> {};

template<class Key, class Default> struct force<invoke<find, facts<>, Key, Default>>:
	force<Default> {};


struct cat_facts {};
template<class... Collections> using cat_facts_t = invoke_t<cat_facts, Collections...>;
template<class... Collections> static constexpr auto cat_facts_v = invoke_v<cat_facts, Collections...>;

template<class... Items1, class... Items2, class... Collections>
struct force<invoke<cat_facts, facts<Items1...>, facts<Items2...>, Collections...>>:
	force<invoke<cat_facts, facts<Items1..., Items2...>, Collections...>> {};

template<class... Items>
struct force<invoke<cat_facts, facts<Items...>>>: force<facts<Items...>> {};

template<> struct force<invoke<cat_facts>>: force<facts<>> {};


namespace impl_::emplace {

template<class Facts1, class Facts2, class Key, class Val> struct emplace {};

}

struct emplace {};
template<class Facts, class Key, class Val> using emplace_t = invoke_t<emplace, Facts, Key, Val>;

template<class Facts, class Key, class Val> struct force<invoke<emplace, Facts, Key, Val>>: force<impl_::emplace::emplace<facts<>, Facts, Key, Val>> {};

template<class... SievedFacts, class K, class V, class... OtherFacts, class Key, class Val>
struct force<impl_::emplace::emplace<facts<SievedFacts...>, facts<K, V, OtherFacts...>, Key, Val>>:
	force<impl_::emplace::emplace<facts<SievedFacts..., K, V>, facts<OtherFacts...>, Key, Val>> {};

template<class... SievedFacts, class V, class... OtherFacts, class Key, class Val>
struct force<impl_::emplace::emplace<facts<SievedFacts...>, facts<Key, V, OtherFacts...>, Key, Val>>:
	force<facts<SievedFacts..., Key, Val, OtherFacts...>> {};

template<class... SievedFacts, class Key, class Val>
struct force<impl_::emplace::emplace<facts<SievedFacts...>, facts<>, Key, Val>>:
	force<facts<SievedFacts..., Key, Val>> {};

}

#endif
