#ifndef QUADRANT__REPEAT_H_22a598f22ba0b9e5559bc973a7e93c2b3b771184
#define QUADRANT__REPEAT_H_22a598f22ba0b9e5559bc973a7e93c2b3b771184

#include "inf.h"
#include "prelude.h"

#include <compare>
#include <concepts>
#include <functional>
#include <limits>
#include <tuple>
#include <type_traits>
#include <utility>

namespace nstd {

namespace tuple {

#if !(__cpp_lib_forward_like >= 202207L)	// Clang

namespace impl_ {

template<class Model, class Forwarded>
inline constexpr decltype(auto) forward_like(Forwarded &&obj) noexcept {
	using BareForwarded = std::remove_const_t<std::remove_reference_t<Forwarded>>;
	if constexpr(std::is_const_v<std::remove_reference_t<Model>>) {
		if constexpr(std::is_lvalue_reference_v<Model &&>) {
			return const_cast<BareForwarded const &>(obj);
		}
		else {
			return const_cast<BareForwarded const &&>(std::move(obj));
		}
	}
	else {
		if constexpr(std::is_lvalue_reference_v<Model &&>) return obj;
		else return std::move(obj);
	}
}

}

#define NSTD_FL_NS impl_

#else

#define NSTD_FL_NS std

#endif

struct car {};
template<class T> using car_t = invoke_t<car, T>;
template<class T> static constexpr auto car_v = invoke_v<car, T>;


struct cdr {};
template<class T> using cdr_t = invoke_t<cdr, T>;
template<class T> static constexpr auto cdr_v = invoke_v<cdr, T>;


struct init {};
template<class T> using init_t = invoke_t<init, T>;
template<class T> static constexpr auto init_v = invoke_v<init, T>;


struct last {};
template<class T> using last_t = invoke_t<last, T>;
template<class T> static constexpr auto last_v = invoke_v<last, T>;


struct map {};
template<class F, class Tuple, class... Tuples> using map_t = invoke_t<map, F, Tuple, Tuples...>;


struct to_tuple {};
template<class T> using to_tuple_t = invoke_t<to_tuple, T>;


template<class T> struct is_getter: std::false_type {};
template<class T> static constexpr auto is_getter_v = is_getter<T>::value;


template<class T> concept getter = is_getter_v<T>;


template<class T> struct size {
	static constexpr auto value = T::size;
};
template<class T> static constexpr auto size_v = size<T>::value;


template<class T> concept tuplelike = requires { std::tuple_size<force_t<T>>::value; };


namespace impl_ {

template<class, class...> struct Type {};
template<class I, class... Ts> using TypeT = call_t<Type, I, Ts...>;

template<class T> using like_a_tuple = value_box<tuplelike<T>>;

struct map {};

template<class F, class Tuple, class... Tuples>
struct map_elements {
	template<std::size_t i> using type = invoke_t<F, std::tuple_element_t<i, Tuple>, std::tuple_element_t<i, Tuples>...>;
};

}

}

template<class T> struct force<invoke<tuple::car, T>>: force<invoke<let, invoke<var::any, var::h>, T, var::h>> {};

template<class T> struct force<invoke<tuple::cdr, T>>: force<invoke<let, invoke<var::any, var::any, var::seq<var::t>>, T, var::t>> {};

template<class T> struct force<invoke<tuple::init, T>>: force<invoke<let, invoke<var::any, var::seq<var::i>, var::any>, T, var::i>> {};

template<class T> struct force<invoke<tuple::last, T>>: force<invoke<let, invoke<var::any, var::seq<var::any>, var::l>, T, var::l>> {};

template<class V, V... vs> struct force<invoke<tuple::to_tuple, std::integer_sequence<V, vs...>>>: force<std::tuple<value_box<vs>...>> {};


template<std::size_t... is, class C, class F, class Tuple, class... Tuples>
struct force<invoke<tuple::impl_::map, std::index_sequence<is...>, C, F, Tuple, Tuples...>>:
	force<C, typename tuple::impl_::map_elements<F, Tuple, Tuples...>::template type<is>...> {};


template<class F, class Tuple, class... Tuples> struct force<invoke<tuple::map, F, Tuple, Tuples...>>:
	force<cond,
		invoke<is_invocation, Tuple, var::any>,
			invoke<let,
				var::c, typename decompose_call<Tuple>::f,
				var::a, typename decompose_call<Tuple>::args,
				var::s, std::tuple_size<var::a>,
				invoke<tuple::impl_::map,
					strict<invoke<type_function_0<std::make_index_sequence>, var::s>>,
					var::c,
					F,
					strict<Tuple>,
					strict<Tuples>...>>,
		invoke<tuple::impl_::map,
			std::make_index_sequence<std::tuple_size_v<Tuple>>,
			fun_box<std::tuple>,
			F,
			strict<Tuple>,
			strict<Tuples>...>> {};


template<class I, class T, class... Ts> struct force<tuple::impl_::Type<I, T, Ts...>>:
	force<cond,
		invoke<conj, tuple::impl_::like_a_tuple<T>, invoke<less, I, std::tuple_size<T>>>,
			invoke<type_function_0<std::tuple_element_t>, strict<I>, strict<T>>,
		tuple::impl_::like_a_tuple<T>,
			call<tuple::impl_::Type, invoke<minus, I, std::tuple_size<T>>, Ts...>, 
		invoke<is_zero, I>,
			T,
		call<tuple::impl_::Type, invoke<dec, I>, Ts...>> {};


namespace tuple {

template<class... Tuples>
class cat {
	template<class T> using Cell = cond_t<std::is_lvalue_reference<T>, T, std::remove_reference_t<T>>;
	using Storage = std::tuple<Cell<Tuples>...>;

	template<class T> static constexpr auto cell_size_v = cond_v<
		is_getter<T>, size<T>,
		impl_::like_a_tuple<T>, std::tuple_size<T>,
		one_box>;

	template<template<std::size_t> class Elem, class Is> using StrictT = invoke_t<map, type_function_0<Elem>, to_tuple_t<Is>>;

	template<class T, std::size_t... is>
	constexpr T getAll(std::index_sequence<is...>) const
	noexcept(noexcept(T{get<is>()...}))
	{
		return {get<is>()...};
	}

	Storage tuples;

public:
	static constexpr auto size = (cell_size_v<std::remove_cvref_t<Tuples>> + ... + 0);

	template<std::size_t i>
	requires(i < size)
	using type = impl_::TypeT<value_box<i>, std::remove_cvref_t<Tuples>...>;

	using tuple_type = StrictT<type, std::make_index_sequence<size>>;

	constexpr cat(Tuples &&...tuples) noexcept(std::is_nothrow_constructible_v<Storage, Tuples...>)
		: tuples(std::forward<Tuples>(tuples)...) {}
	
#if __cpp_explicit_this_parameter >= 202110L	// non-Clang
	template<std::size_t i, std::size_t j = 0, typename This>
	requires(i < size)
	constexpr decltype(auto) get(this This &&self) noexcept {
		using Cell = std::remove_cvref_t<std::tuple_element_t<j, Storage>>;
		static constexpr auto sz = cell_size_v<Cell>;
		if constexpr(i < sz) {
			if constexpr(tuplelike<Cell>) {
				if constexpr(getter<Cell>) {
					return std::get<j>(NSTD_FL_NS::forward_like<This>(self.tuples)).template get<i>();
				}
				else return std::get<i>(std::get<j>(NSTD_FL_NS::forward_like<This>(self.tuples)));
			}
			else return std::get<j>(NSTD_FL_NS::forward_like<This>(self.tuples));
		}
		else return std::forward<This>(self).template get<i - sz, j + 1>();
	}
#else

#define GET_DEFINITION(ref)												\
	template<std::size_t i, std::size_t j = 0>									\
	requires(i < size)												\
	constexpr decltype(auto) get() ref noexcept {									\
		using Cell = std::remove_cvref_t<std::tuple_element_t<j, Storage>>;					\
		using This = cat ref;											\
		static constexpr auto sz = cell_size_v<Cell>;								\
		if constexpr(i < sz) {											\
			if constexpr(tuplelike<Cell>) {									\
				if constexpr(getter<Cell>) {								\
					return std::get<j>(NSTD_FL_NS::forward_like<This>(tuples)).template get<i>();	\
				}											\
				else return std::get<i>(std::get<j>(NSTD_FL_NS::forward_like<This>(tuples)));			\
			}												\
			else return std::get<j>(NSTD_FL_NS::forward_like<This>(tuples));						\
		}													\
		else return std::forward<This>(*this).template get<i - sz, j + 1>();					\
	}

	GET_DEFINITION(&);
	GET_DEFINITION(&&);
	GET_DEFINITION(const &);
	GET_DEFINITION(const &&);
#undef GET_DEFINITION
#endif

	constexpr operator tuple_type() const
	noexcept(noexcept(getAll<tuple_type>(std::make_index_sequence<size>{})))
	{
		return getAll<tuple_type>(std::make_index_sequence<size>{});
	}

	constexpr tuple_type to_tuple() const
	noexcept(noexcept((tuple_type)*this))
	{
		return *this;
	}
};

template<class... Tuples> cat(Tuples &&...) -> cat<Tuples...>;

template<class... Tuples> struct is_getter<cat<Tuples...>>: std::true_type {};


template<class T, auto count = inf{}>
struct repeat {
	static_assert(std::same_as<decltype(count), inf> || std::convertible_to<decltype(count), std::size_t>);

	using size_type = std::conditional_t<std::same_as<decltype(count), inf>, inf, std::size_t>;
	static constexpr size_type size = count;

	constexpr repeat(T &&t) noexcept(std::is_nothrow_constructible_v<T, T>): t(std::forward<T>(t)) {}

	template<std::size_t i>
	requires(i < count)
	using type = T;

#if __cpp_explicit_this_parameter >= 202110L	// non-Clang
	template<std::size_t i, class This>
	requires(i < count)
	constexpr decltype(auto) get(this This &&self) noexcept {
		return NSTD_FL_NS::forward_like<This>(self.t);
	}
#else
#define GET_DEFINITION(ref)					\
	template<std::size_t i>					\
	requires(i < count)					\
	constexpr decltype(auto) get() ref noexcept {		\
		using This = repeat ref;			\
		return NSTD_FL_NS::forward_like<This>(t);	\
	}

	GET_DEFINITION(&);
	GET_DEFINITION(&&);
	GET_DEFINITION(const &);
	GET_DEFINITION(const &&);
#undef GET_DEFINITION
#endif

#undef NSTD_FL_NS

private:
	T t;
};

template<class T, auto count> struct is_getter<repeat<T, count>>: std::true_type {};

template<auto count, class T>
inline constexpr repeat<T, count> make_repeat(T &&t)
noexcept(std::is_nothrow_constructible_v<repeat<T, count>, T>)
{
	return {std::forward<T>(t)};
}

}

}

namespace std {

template<nstd::tuple::getter T>
struct tuple_size<T>: nstd::tuple::size<T> {};

template<std::size_t i, nstd::tuple::getter T>
struct tuple_element<i, T>
{
	using type = typename T::template type<i>;
};

template<size_t i, nstd::tuple::getter T>
inline constexpr tuple_element_t<i, T> &get(T &r) noexcept
{
	return r.template get<i>();
}

template<size_t i, nstd::tuple::getter T>
inline constexpr tuple_element_t<i, T> const &get(T const &r) noexcept
{
	return r.template get<i>();
}

template<size_t i, nstd::tuple::getter T>
requires(!is_lvalue_reference_v<tuple_element_t<i, T>>)
inline constexpr tuple_element_t<i, T> &&get(T &&r) noexcept
{
	return std::move(r).template get<i>();
}

template<size_t i, nstd::tuple::getter T>
requires(!is_lvalue_reference_v<tuple_element_t<i, T>>)
inline constexpr tuple_element_t<i, T> const &&get(T const &&r) noexcept
{
	return std::move(r).template get<i>();
}

}

#endif
