#ifndef QUADRANT__PRELUDE_H_bf01e8abc3588475e1a63cd472fc076ee8e94c3c
#define QUADRANT__PRELUDE_H_bf01e8abc3588475e1a63cd472fc076ee8e94c3c

#include "bits/prelude/box.h"
#include "bits/prelude/bool.h"
#include "bits/prelude/cond.h"
#include "bits/prelude/combinators.h"
#include "bits/prelude/facts.h"
#include "bits/prelude/imperative.h"
#include "bits/prelude/let.h"
#include "bits/prelude/op.h"
#include "bits/prelude/type_function.h"
#include "bits/prelude/var.h"

#include <type_traits>

namespace nstd {

struct first {};
template<class... Ts> using first_t = invoke_t<first, Ts...>;
template<class... Ts> inline constexpr auto first_v = invoke_v<first, Ts...>;

template<class T, class... Ts> struct force<invoke<first, T, Ts...>>: force<T> {};

}

#endif
